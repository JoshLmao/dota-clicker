﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveManager
{
    public delegate void OnLoadedSaveFile(SaveFileDto saveFile);
    public static event OnLoadedSaveFile LoadedSaveFile;
    public delegate void OnLoadedConfigFile(ConfigDto config);
    public static event OnLoadedConfigFile LoadedConfigFile;

    public readonly static string FILE_PATHS = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\My Games\\DotAClickerVR\\";
    public readonly static string SAVE_FILE = "SaveFile.json";
    public readonly static string CONFIG_FILE = "ConfigFile.json";

    public static SaveFileDto CurrentSaveFile;
    public static ConfigDto CurrentConfigFile;

    private static string SAVE_FILE_LOCATION { get { return FILE_PATHS + SAVE_FILE; } }
    private static string CONFIG_LOCATION { get { return FILE_PATHS + CONFIG_FILE; } }

    public void LoadProgress()
    {
        if (!File.Exists(SAVE_FILE_LOCATION))
            return;

        try
        {
            //Deserialize existing file
            string file = File.ReadAllText(SAVE_FILE_LOCATION);
            SaveFileDto loadedSaveFile = JsonConvert.DeserializeObject<SaveFileDto>(file);
            CurrentSaveFile = loadedSaveFile;
        }
        catch (Exception e)
        {
            Debug.LogError("Can't load save file - " + e.ToString());
            CurrentSaveFile = new SaveFileDto();
        }

        //if (LoadedSaveFile != null)
        //    LoadedSaveFile.Invoke(CurrentSaveFile);

        //If it hasnt been set in menus
        //CurrentPlayerName = CurrentSaveFile.PlayerName;

        //OnLoadedSave(CurrentSaveFile);
    }

    public static SaveFileDto LoadSave()
    {
        return null;
    }

    public static void SaveSave()
    {
        //Check if folders & file exists
        //CheckSaveFileFolders();

        //Add current playtime to total play time
        //m_totalPlayTime += Time.realtimeSinceStartup;
        bool hasSaveFile = false;

        if (CurrentSaveFile != null)
            hasSaveFile = true;

        //Save data
        //SaveFileDto saveFile = new SaveFileDto()
        //{
        //    PlayerName = CurrentPlayerName,
        //    RadiantSide = new RadiantSideDto()
        //    {
        //        TotalGold = TotalGold,
        //        RoshanEvents = m_canDoRoshanEvent,
        //    },
        //    SessionStats = new StatsDto()
        //    {
        //        TotalPlayTime = hasSaveFile && CurrentSaveFile.SessionStats != null ? CurrentSaveFile.SessionStats.TotalPlayTime += m_totalPlayTime : m_totalPlayTime,
        //        ClickCount = ClickCount,
        //        ItemStats = new ItemStatsDto()
        //        {
        //            IronBranchCount = modifierController.IronBranchCount,
        //            ClarityCount = modifierController.ClarityCount,
        //            MagicStickCount = modifierController.MagicStickCount,
        //            QuellingBladeCount = modifierController.QuellingBladeCount,
        //            MangoCount = modifierController.MangoCount,
        //            PowerTreadsCount = modifierController.PowerTreadsCount,
        //            BottleCount = modifierController.BottleCount,
        //            BlinkDaggerCount = modifierController.BlinkDaggerCount,
        //            HyperstoneCount = modifierController.HyperstoneCount,
        //            BloodstoneCount = modifierController.BloodstoneCount,
        //            ReaverCount = modifierController.ReaverCount,
        //            DivineRapierCount = modifierController.DivineRapierCount,
        //            RecipeCount = modifierController.RecipeCount,
        //        }
        //    },
        //    Achievements = new AchievementsDto()
        //    {
        //        Earn625Gold = m_achievementEvents.Earn625GoldStatus,
        //        Earn6200Gold = m_achievementEvents.Earn6200GoldStatus,
        //        Earn15000Gold = m_achievementEvents.Earn15000GoldStatus,
        //        Earn100000Gold = m_achievementEvents.Earn100000GoldStatus,
        //        EarnMillionGold = m_achievementEvents.EarnMillionGoldStatus,

        //        ClickOnce = m_achievementEvents.ClickOnceStatus,
        //        ClickFiveHundred = m_achievementEvents.ClickFiveHundredStatus,
        //        ClickThousand = m_achievementEvents.ClickThousandStatus,
        //        ClickFifteenThousand = m_achievementEvents.ClickFifteenThousandStatus,
        //        ClickFiftyThousand = m_achievementEvents.ClickFiftyThousandStatus,

        //        BuyAnAbility = m_achievementEvents.BuyAnAbilityStatus,
        //        BuyAllAbilitiesForAHero = m_achievementEvents.BuyAllAbilitiesForAHeroStatus,
        //        BuyAllAbilities = m_achievementEvents.BuyAllAbilitiesStatus,

        //        BuyAManager = m_achievementEvents.BuyAManagerStatus,
        //        BuyAllManagers = m_achievementEvents.BuyAllManagersStatus,

        //        BuyAnItem = m_achievementEvents.BuyAnItemStatus,
        //        BuyEachItemOnce = m_achievementEvents.BuyEachItenOnceStatus,

        //        DefeatRoshan = m_achievementEvents.DefeatRoshanStatus,
        //        DefeatRoshanTenTimes = m_achievementEvents.DefeatRoshanTenTimesStatus,

        //        TheAegisIsMine = m_achievementEvents.AegisIsMineStatus,
        //        CheeseGromit = m_achievementEvents.CheeseGromitStatus,

        //        TheClosestYoullGetToABattleCup = m_achievementEvents.ClosestYoullGetStatus,
        //        WhenDidEGThrowLast = m_achievementEvents.EGThrowLastStatus,
        //        TheManTheMythTheLegend = m_achievementEvents.ManMythLegendStatus,
        //    }
        //};

        /*
         * Hero Order in List:
         * 0 = Alchemist, 1 = Ogre, 2 = Tusk, 3 = Io, 4 = AntiMagi, 5 = Sven, 6 = Phoenix, 7 = Rubick
         */
        //saveFile.RadiantSide.Heroes = new List<HeroDto>();
        //for (int i = 0; i < SceneHeroes.Count; i++)
        //{
        //    saveFile.RadiantSide.Heroes.Add(new HeroDto()
        //    {
        //        HeroName = SceneHeroes[i].HeroName,
        //        ClickersBought = SceneHeroes[i].ClickerMultiplier,
        //        ClickerTimeRemaining = CorrectTimeRemaining(i),

        //        Ability1Level = SceneHeroes[i].Ability1Level,
        //        Ability1UseCount = SceneHeroes[i].Ability1UseCount,
        //        Ability1RemainingTime = SceneHeroes[i].m_ability1ClickTime != DateTime.MinValue ? (DateTime.Now - SceneHeroes[i].m_ability1ClickTime).TotalSeconds : 0,
        //        Ability1InUse = SceneHeroes[i].Ability1InUse,

        //        Ability2Level = SceneHeroes[i].Ability2Level,
        //        Ability2UseCount = SceneHeroes[i].Ability2UseCount,
        //        Ability2RemainingTime = SceneHeroes[i].m_ability2ClickTime != DateTime.MinValue ? (DateTime.Now - SceneHeroes[i].m_ability2ClickTime).TotalSeconds : 0,
        //        Ability2InUse = SceneHeroes[i].Ability2InUse,

        //        ModifierActive = SceneHeroes[i].m_currentModifierRoutineStarted == DateTime.MinValue ? false : true,
        //        CurrentModifier = SceneHeroes[i].m_currentModifier,
        //        ModifierTimeRemaining = SceneHeroes[i].m_currentModifierRoutineStarted != DateTime.MinValue ? CalculateModifierTimeRemaining(SceneHeroes[i].m_currentModifierRoutineStarted, SceneHeroes[i].m_currentModifierTotalTime) : 0,

        //        HasManager = SceneHeroes[i].HasManager,
        //    });
        //}

        //saveFile.Roshan = new RoshanDto()
        //{
        //    DefeatCount = RoshanEventCount,
        //    DurationTillNextSpawn = m_roshanEventInProgress ? 0 : m_secondsToRoshanEvent,
        //    CanDoRoshanEvents = m_canDoRoshanEvent,
        //    RoshanHealth = m_activeRoshan != null ? m_activeRoshan.CurrentHealth : 0,
        //    GoldOnStart = m_activeRoshan != null ? m_activeRoshan.m_playerGoldOnStart : 0,
        //    TimeRemaining = m_activeRoshan != null && m_activeRoshan.TimeRemaining > 0 ? m_activeRoshan.TimeRemaining : 0,
        //};

        try
        {
            //string json = JsonConvert.SerializeObject(saveFile, Formatting.Indented);
            //File.WriteAllText(SAVE_FILE_LOCATION, json);
        }
        catch (Exception e)
        {
            Debug.Log("Exception occured: - Can't save file");
        }
    }

    public static ConfigDto LoadConfig()
    {
        ConfigDto config;

        if (!Directory.Exists(FILE_PATHS))
            Directory.CreateDirectory(FILE_PATHS);

        if (!File.Exists(CONFIG_LOCATION))
        {
            //File doesnt exist. Create default file with content
            var stream = File.Create(CONFIG_LOCATION);
            stream.Close();

            config = new ConfigDto();
            //if (m_options != null)
            //{
            //    //Set defaults in quotes for other users
            //    config = GetLatestConfigValues();
            //}

            config.TwitchUsername = "";
            config.TwitchAuthCode = "";

            string json = JsonConvert.SerializeObject(config, Formatting.Indented);
            File.WriteAllText(CONFIG_LOCATION, json);

            CurrentConfigFile = config;

            //if (LoadedConfigFile != null && config != null)
            //    LoadedConfigFile.Invoke(config);

            return config;
        }
        else
        {
            //File exists. Load it
            string content = File.ReadAllText(CONFIG_LOCATION);
            try
            {
                config = JsonConvert.DeserializeObject<ConfigDto>(content);
            }
            catch (Exception e)
            {
                Console.WriteLine("Can't deserialize Config File");
                return null;
            }


            //if (LoadedConfigFile != null)
            //    LoadedConfigFile.Invoke(config);

            return config;
        }
    }

    public static void SaveSaveFile()
    {
        if (CurrentConfigFile == null)
        {
            Debug.LogError("Can't save config file because it's null");
            return;
        }

        //CheckSaveFileFolders();
        //CurrentConfigFile = GetLatestConfigValues();

        try
        {
            var json = JsonConvert.SerializeObject(CurrentConfigFile, Formatting.Indented);
            File.WriteAllText(CONFIG_LOCATION, json);
        }
        catch (Exception e)
        {
            Debug.Log("Can't save config file - " + e.Message);
        }
    }

    void CheckSaveFileFolders()
    {
        if (!Directory.Exists(FILE_PATHS))
            Directory.CreateDirectory(FILE_PATHS);

        if (!File.Exists(SAVE_FILE_LOCATION))
            File.Create(SAVE_FILE_LOCATION).Close();
    }
}
