﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// World context menu build for selling items in hero inventory
/// </summary>
public class WorldSpaceContextMenu : MonoBehaviour
{
    public ItemDto Item = null;
    public int SlotIndex = -1;
    public Hero Hero = Hero.Unknown;

    [SerializeField]
    private Button m_close = null;

    [SerializeField]
    private Button m_sell = null;

    private EventManager m_events;

    void Start()
    {
        if(m_close != null)
            m_close.onClick.AddListener(OnClose);

        if (m_sell != null)
            m_sell.onClick.AddListener(OnSellItem);

        m_events = SceneController.GetEventManager();
        Canvas canvas = GetComponent<Canvas>();
        canvas.overrideSorting = true;
        canvas.sortingOrder = 1;
    }

    public void Configure(ItemDto item, int slotIndex, Hero hero)
    {
        Item = item;
        SlotIndex = slotIndex;
        Hero = hero;
    }

    private void OnSellItem()
    {
        m_events.TriggerEvent(new OnSellItem(Item, SlotIndex, Hero));
        Destroy(this.gameObject);
    }

    private void OnClose()
    {
        Destroy(this.gameObject);
    }
}
