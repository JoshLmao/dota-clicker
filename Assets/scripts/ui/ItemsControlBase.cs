﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Spotify4Unity
/// Base class for creating a vertical list of prefabs which can be interacted with
/// (Make sure you read the Wiki documentation for all information & help!)
/// </summary>
/// <typeparam name="ListElementT">The data class that will be used to populate the prefab with data</typeparam>
public class ItemsControlBase<ListElementT> : MonoBehaviour where ListElementT : class
{
    [SerializeField, Tooltip("The prefab for one element of the control")]
    protected GameObject m_prefab = null;

    [SerializeField, Tooltip("The canvas that will hold all instantiated prefabs & be resized to fit them")]
    protected RectTransform m_resizeCanvas = null;

    [SerializeField, Tooltip("The ScrollRect that will control scrolling the list, usually the parent GameObject of the ResizeCanvas")]
    protected ScrollRect m_scrollRect = null;

    [SerializeField, Tooltip("The amount of pixels inbetween each prefab element in the vertical list")]
    protected float m_uiSpacingY = 0f;

    [SerializeField, Tooltip("Should the list be optimised by disabling elements not in view?")]
    protected bool m_optimiseList = true;

    [SerializeField, Tooltip("Maximum limit of how many entries in the list. 0 for unlimited")]
    protected int m_listLimit = 1000;

    [SerializeField]
    ScrollRect.MovementType m_scrollType = ScrollRect.MovementType.Clamped;

    protected float m_prefabHeight { get { return m_prefab != null ? m_prefab.GetComponent<RectTransform>().rect.height : 0f; } }

    protected List<GameObject> m_instantiatedPrefabs = null;
    protected Coroutine m_updateRoutine = null;

    private bool m_updateListElementStates = false;

    #region MonoBehaviours
    protected virtual void Start()
    {
        if (m_resizeCanvas != null)
            DestroyChildren(m_resizeCanvas);

        if (m_scrollRect != null)
        {
            m_scrollRect.movementType = m_scrollType;
            m_scrollRect.verticalScrollbar.onValueChanged.AddListener(OnScrollChanged);
        }
    }

    protected virtual void Update()
    {
        if (m_updateListElementStates)
        {
            //Set visible states on first tick after Unity sets the UI, otherwise positions are incorrect
            UpdateVisibleElements(m_prefabHeight);
            m_updateListElementStates = false;
        }
    }

    protected virtual void OnDestroy()
    {
        if (m_updateRoutine != null)
        {
            StopCoroutine(m_updateRoutine);
            m_updateRoutine = null;
        }
    }
    #endregion

    /// <summary>
    /// Updates the list UI in a vertical pattern
    /// </summary>
    /// <param name="dataList">The list of data to populate each prefab with</param>
    protected virtual void UpdateUI(List<ListElementT> dataList)
    {
        DestroyChildren(m_resizeCanvas);

        if (dataList == null || dataList != null && dataList.Count <= 0)
            return;

        if (m_prefab == null)
        {
            Debug.LogError($"Can't populate list on '{this.name}' since no prefab has been specified");
            return;
        }

        //Remove any excess data we don't need if limit is above 0
        if (m_listLimit > 0 && dataList.Count > m_listLimit)
            dataList.RemoveRange(m_listLimit, dataList.Count - 1 - m_listLimit);
            
        List<GameObject> createdPrefabs = new List<GameObject>();
        float yPos = 0f;
        float newCanvasHeight = 0f;
        foreach (ListElementT data in dataList)
        {
            if (data == null)
                continue;

            GameObject instPrefab = SetListElement(data, yPos);
            float incrementAmount = instPrefab.transform.GetComponent<RectTransform>().rect.height + m_uiSpacingY;
            yPos -= incrementAmount;
            newCanvasHeight += incrementAmount;

            createdPrefabs.Add(instPrefab);
        }

        SetScrollView(ref m_resizeCanvas, ref m_scrollRect, new Vector2(0, newCanvasHeight), m_prefab.GetComponent<RectTransform>().rect.height);

        m_instantiatedPrefabs = createdPrefabs;
        OnUIUpdateFinished();
    }

    /// <summary>
    ///  Updates the list UI in a vertical pattern using Unity's Couroutines
    /// </summary>
    /// <param name="dataList"></param>
    protected virtual void UpdateUICoroutine(List<ListElementT> dataList)
    {
        DestroyChildren(m_resizeCanvas);

        if (dataList == null || dataList != null && dataList.Count <= 0)
            return;

        //Remove any excess data we don't need if limit is above 0
        if (m_listLimit > 0 && dataList.Count > m_listLimit)
            dataList.RemoveRange(m_listLimit, dataList.Count - 1 - m_listLimit);

        m_updateRoutine = StartCoroutine(UpdateRoutine(dataList));
    }

    protected virtual System.Collections.IEnumerator UpdateRoutine(List<ListElementT> dataList)
    {
        if (m_prefab == null)
        {
            Debug.LogError($"Can't populate list on '{this.name}' since no prefab has been specified");
            yield return null;
        }

        List<GameObject> createdPrefabs = new List<GameObject>();
        float yPos = 0f;
        float newCanvasHeight = 0f;
        foreach (ListElementT data in dataList)
        {
            if (data == null)
                continue;

            GameObject instPrefab = SetListElement(data, yPos);
            float incrementAmount = instPrefab.transform.GetComponent<RectTransform>().rect.height + m_uiSpacingY;
            yPos -= incrementAmount;
            newCanvasHeight += incrementAmount;

            createdPrefabs.Add(instPrefab);

            yield return new WaitForEndOfFrame();
        }

        SetScrollView(ref m_resizeCanvas, ref m_scrollRect, new Vector2(0, newCanvasHeight), m_prefab.GetComponent<RectTransform>().rect.height);

        m_instantiatedPrefabs = createdPrefabs;
        OnUIUpdateFinished();

        m_updateRoutine = null;
    }

    private GameObject SetListElement(ListElementT data, float yPos)
    {
        GameObject instPrefab = Instantiate(m_prefab, m_resizeCanvas);

        //Set the information needed inside the prefab
        SetPrefabInfo(instPrefab, data);

        //Set Y position of instantiated prefab
        RectTransform rect = instPrefab.GetComponent<RectTransform>();
        rect.localPosition = new Vector3(rect.localPosition.x, yPos, rect.localPosition.z);

        //Set width and height to original prefab
        Rect original = m_prefab.GetComponent<RectTransform>().rect;
        rect.sizeDelta = new Vector2(original.width, original.height);

        return instPrefab;
    }

    /// <summary>
    /// Sets the canvas to the target size and right position, and sets the ScrollRect
    /// </summary>
    /// <param name="canvas">The canvas</param>
    /// <param name="scrollRect">The scroll rect of the canvas</param>
    /// <param name="canvasSize">The target new size of the canvas</param>
    /// <param name="scrollSensitivity">The amount to scroll by on one scroll</param>
    protected void SetScrollView(ref RectTransform canvas, ref ScrollRect scrollRect, Vector2 canvasSize, float scrollSensitivity)
    {
        //Set canvas new size
        canvas.sizeDelta = canvasSize;
        //Set scrollbar position with canvas position
        canvas.localPosition = new Vector3(canvas.localPosition.x, -(canvas.rect.height / 2), canvas.localPosition.z);
        //Set sensitivity to scroll 1 track every scroll wheel click
        scrollRect.scrollSensitivity = scrollSensitivity;
        //ScrollRect to top position
        scrollRect.verticalNormalizedPosition = 1;
    }

    /// <summary>
    /// Destroys all children of the parent transform, won't destroy the parent transform passed through
    /// </summary>
    /// <param name="parent">The parent Transform containing children to destroy</param>
    protected void DestroyChildren(Transform parent)
    {
        List<Transform> children = parent.GetComponentsInChildren<Transform>().ToList();
        if (children.Contains(parent))
            children.Remove(parent);

        if (children.Count > 0)
        {
            foreach (Transform child in children)
                GameObject.Destroy(child.gameObject);
        }

        if (m_instantiatedPrefabs != null)
        {
            m_instantiatedPrefabs.Clear();
            m_instantiatedPrefabs = null;
        }
    }

    /// <summary>
    /// List optimization to not draw every single prefab object if it isn't in view 
    /// </summary>
    /// <param name="offset">The amount of pixels to add as offset to the canvas corners to draw extra UI above/below</param>
    protected void UpdateVisibleElements(float offset = 0f)
    {
        if (!m_optimiseList)
            return;

        if (m_instantiatedPrefabs != null)
        {
            Vector3[] canvasV = new Vector3[4];
            m_scrollRect.GetComponent<RectTransform>().GetWorldCorners(canvasV);

            foreach (GameObject o in m_instantiatedPrefabs)
            {
                Vector3[] objectV = new Vector3[4];
                o.GetComponent<RectTransform>().GetWorldCorners(objectV);

                bool isInView = false;
                for (var i = 0; i < 4; i++)
                {
                    //Compare canvas world corners with the prefab world corners,
                    //If in view, enable and show
                    if (objectV[1].y <= canvasV[1].y + offset                //Top Left
                        && objectV[0].y >= canvasV[0].y - offset             //Btm Left
                        && objectV[2].y <= canvasV[2].y + offset             //Top Right
                        && objectV[3].y >= canvasV[3].y - offset)            //Btm Right
                    {
                        isInView = true;
                    }
                }

                if (isInView != o.activeInHierarchy)
                    o.SetActive(isInView);
            }
        }
    }

    /// <summary>
    /// Populate each prefab in the list with it's data
    /// </summary>
    /// <param name="instantiatedPrefab">The currently instantiated prefab</param>
    /// <param name="data">The current data the list is on</param>
    protected virtual void SetPrefabInfo(GameObject instantiatedPrefab, ListElementT data)
    {
    }

    /// <summary>
    /// Callback for when the UI has finished populating the list
    /// </summary>
    protected virtual void OnUIUpdateFinished()
    {
        m_updateListElementStates = true;
    }

    protected void OnScrollChanged(float position)
    {
        UpdateVisibleElements(m_prefabHeight);
    }
}
