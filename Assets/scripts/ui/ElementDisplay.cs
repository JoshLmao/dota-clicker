﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ElementDisplay : MonoBehaviour
{
    public struct UIInfo
    {
        public string Title;
        public string Description;
        public string Cost;
        public Sprite Sprite;
        public Vector2 SpriteSize;
    }

    [SerializeField]
    private Image m_image;

    [SerializeField]
    private TextMeshProUGUI m_title;

    [SerializeField]
    private TextMeshProUGUI m_description;

    [SerializeField]
    private TextMeshProUGUI m_cost;

    [SerializeField]
    private Button m_buyBtn;

    public void SetInfo<T>(UIInfo info, T item, Action<T> btnCallback) where T : class
    {
        m_title.text = info.Title;
        m_description.text = info.Description;
        m_cost.text = info.Cost;

        m_image.sprite = info.Sprite;
        m_image.rectTransform.sizeDelta = info.SpriteSize;

        m_buyBtn.onClick.AddListener(() => btnCallback?.Invoke(item));
    }
}
