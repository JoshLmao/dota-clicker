﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickerCouriersList : ItemsControlBase<ClickerCourierDto>
{
    public List<ClickerCourierDto> Couriers { get; set; }

    [SerializeField]
    private CourierService m_service = null;

    [SerializeField]
    private Vector2 m_imgSize = Vector2.zero;

    private SceneController m_scene = null;
    private CourierPlacer m_courierPlacer = null;
    private AudioSource m_source = null;

    private void Awake()
    {
        m_scene = FindObjectOfType<SceneController>();
        m_courierPlacer = FindObjectOfType<CourierPlacer>();
        m_source = Utilities.GetAddComponent<AudioSource>(this.gameObject);
    }

    protected override void Start()
    {
        base.Start();

        Couriers = GetCouriers();
        UpdateUI(Couriers);
    }

    protected override void SetPrefabInfo(GameObject instantiatedPrefab, ClickerCourierDto data)
    {
        base.SetPrefabInfo(instantiatedPrefab, data);

        ElementDisplay e = instantiatedPrefab.GetComponent<ElementDisplay>();
        e.SetInfo<ClickerCourierDto>(new ElementDisplay.UIInfo()
        {
            Title = data.Name,
            Description = data.Description,
            Cost = data.Cost.ToString(),
            Sprite = data.Image,
            SpriteSize = m_imgSize,
        }, data, (i) => BuyCourier(i));
    }

    private List<ClickerCourierDto> GetCouriers()
    {
        string path = "images/ui/courier-icons/";
        List<ClickerCourierDto> crows = new List<ClickerCourierDto>()
        {
            {
                new ClickerCourierDto()
                {
                    Name = "Donkey Radiant",
                    Description = Constants.DonkeyRadiant.ToString(),
                    Image = Resources.Load<Sprite>(path + "donkey-radiant"),
                    Courier = Courier.DonkeyRadiant,

                    Cost = Constants.DonkeyRadiant.Cost,
                    AtkSpeedBonus = Constants.DonkeyRadiant.AttackSpeed,
                    DamageBonus = Constants.DonkeyRadiant.Damage,
                }
            },
            {
                new ClickerCourierDto()
                {
                    Name = "Donkey Dire",
                    Description = Constants.DonkeyDire.ToString(),
                    Image = Resources.Load<Sprite>(path + "donkey-dire"),
                    Courier = Courier.DonkeyDire,

                    Cost = Constants.DonkeyDire.Cost,
                    AtkSpeedBonus = Constants.DonkeyDire.AttackSpeed,
                    DamageBonus = Constants.DonkeyDire.Damage,
                }
            },
            {
                new ClickerCourierDto()
                {
                    Name = "Trapjaw",
                    Description = Constants.TrapJaw.ToString(),
                    Image = Resources.Load<Sprite>(path + "trapjaw"),
                    Courier = Courier.Trapjaw,

                    Cost = Constants.TrapJaw.Cost,
                    AtkSpeedBonus = Constants.TrapJaw.AttackSpeed,
                    DamageBonus = Constants.TrapJaw.Damage,
                }
            },
            {
                new ClickerCourierDto()
                {
                    Name = "Doom Demihero",
                    Description = Constants.DoomDemihero.ToString(),
                    Image = Resources.Load<Sprite>(path + "doom-demihero"),
                    Courier = Courier.DoomDemihero,

                    Cost = Constants.DoomDemihero.Cost,
                    AtkSpeedBonus = Constants.DoomDemihero.AttackSpeed,
                    DamageBonus = Constants.DoomDemihero.Damage,
                }
            },
            {
                new ClickerCourierDto()
                {
                    Name = "Baby Wyvern",
                    Description = Constants.BabyWyvern.ToString(),
                    Image = Resources.Load<Sprite>(path + "baby-wyvern"),
                    Courier = Courier.BabyWyvern,

                    Cost = Constants.BabyWyvern.Cost,
                    AtkSpeedBonus = Constants.BabyWyvern.AttackSpeed,
                    DamageBonus = Constants.BabyWyvern.Damage,
                }
            },
            {
                new ClickerCourierDto()
                {
                    Name = "Baby Roshan",
                    Description = Constants.BabyRoshan.ToString(),
                    Image = Resources.Load<Sprite>(path + "baby-roshan"),
                    Courier = Courier.BabyRoshan,

                    Cost = Constants.BabyRoshan.Cost,
                    AtkSpeedBonus = Constants.BabyRoshan.AttackSpeed,
                    DamageBonus = Constants.BabyRoshan.Damage,
                }
            }
        };
        return crows;
    }

    private void BuyCourier(ClickerCourierDto courier)
    {
        if (!m_scene.CanBuy(courier.Cost))
        {
            Debug.LogError($"Can't buy courier '{courier.Name}'");
            m_source.PlayOneShot(GlobalSoundManager.GetInvalidSound());
            return;
        }

        if (m_courierPlacer.HasCourierWaitingSelection())
        {
            Debug.Log("Can't buy another courier when waiting to place previous one");
            m_source.PlayOneShot(GlobalSoundManager.GetInvalidSound());
            return;
        }

        m_source.PlayOneShot(GlobalSoundManager.GetShopBuySound());
        m_scene.RemoveGold(courier.Cost);
        m_service.BuyCourier(courier);
    }
}
