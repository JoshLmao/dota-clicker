﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class HeroItemsList : ItemsControlBase<ItemDto>
{
    public List<ItemDto> Items { get; set; }

    [HideInInspector]
    public int TotalItemsBought = 0;

    /// <summary>
    /// Dict for keeping track of how many times the item has been bought
    /// </summary>
    public Dictionary<string, int> BuyCounts = new Dictionary<string, int>();

    public ItemPrefabDict PrefabDictionary = null;

    [SerializeField]
    private PlayerItemsTray m_playerItems;
    [SerializeField]
    private Transform m_targetSpawn;

    [SerializeField]
    private Vector2 m_imgSize = Vector2.zero;

    private SceneController m_scene = null;
    private AudioSource m_source = null;

    private void Awake()
    {
        m_scene = FindObjectOfType<SceneController>();

        m_source = GetComponent<AudioSource>();
        if(m_source == null)
            m_source = this.gameObject.AddComponent<AudioSource>();
    }

    protected override void Start()
    {
        base.Start();

        Items = GetDefaultItems();
        UpdateUI(Items);
    }

    private List<ItemDto> GetDefaultItems()
    {
        string path = "Images/UI/items-icons/";
        List<ItemDto> items = new List<ItemDto>()
        {
            {
                new ItemDto()
                {
                    Name = "Iron Branch",
                    Description = Constants.IronBranch.ToString(), //"A simple iron branch"
                    Image = Resources.Load<Sprite>(path + "Iron_Branch_icon"),
                    ItemPrefab = PrefabDictionary[Item.IronBranch],

                    Cost = Constants.IronBranch.Cost,
                    AtkSpeedBonus = Constants.IronBranch.AttackSpeed,
                    DamageBonus = Constants.IronBranch.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Quelling Blade",
                    Description = Constants.QuellingBlade.ToString(), //"The axe of the fallen gnome"
                    Image = Resources.Load<Sprite>(path + "Quelling_Blade_icon"),
                    ItemPrefab = PrefabDictionary[Item.QuellingBlade],

                    Cost = Constants.QuellingBlade.Cost,
                    AtkSpeedBonus = Constants.QuellingBlade.AttackSpeed,
                    DamageBonus = Constants.QuellingBlade.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Power Treads",
                    Description = Constants.PowerTreads.ToString(), // "A pair of tough-skinned boots that change to meet the demands of the wearer",
                    Image = Resources.Load<Sprite>(path + "Power_Treads_icon"),
                    ItemPrefab = PrefabDictionary[Item.PowerTreads],

                    Cost = Constants.PowerTreads.Cost,
                    AtkSpeedBonus = Constants.PowerTreads.AttackSpeed,
                    DamageBonus = Constants.PowerTreads.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Bottle",
                    Description = Constants.Bottle.ToString(), // "An old bottle that survived the ages, the contents placed inside become enchanted",
                    Image = Resources.Load<Sprite>(path + "Bottle_icon"),
                    ItemPrefab = PrefabDictionary[Item.Bottle],

                    Cost = Constants.Bottle.Cost,
                    AtkSpeedBonus = Constants.Bottle.AttackSpeed,
                    DamageBonus = Constants.Bottle.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Blink Dagger",
                    Description =  Constants.BlinkDagger.ToString(), // "The fabled dagger used by the fastest assassin ever to walk the lands",
                    Image = Resources.Load<Sprite>(path + "Blink_Dagger_icon"),
                    ItemPrefab = PrefabDictionary[Item.BlinkDagger],

                    Cost = Constants.BlinkDagger.Cost,
                    AtkSpeedBonus = Constants.BlinkDagger.AttackSpeed,
                    DamageBonus = Constants.BlinkDagger.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Hyperstone",
                    Description = Constants.Hyperstone.ToString(), // "A mystical, carved stone that boosts the fervor of the holder",
                    Image = Resources.Load<Sprite>(path + "Hyperstone_icon"),
                    ItemPrefab = PrefabDictionary[Item.HyperStone],

                    Cost = Constants.Hyperstone.Cost,
                    AtkSpeedBonus = Constants.Hyperstone.AttackSpeed,
                    DamageBonus = Constants.Hyperstone.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Bloodstone",
                    Description = Constants.Hyperstone.ToString(), // "The Bloodstone's bright ruby color is unmistakable on the battlefield, as the owner seems to habe infinte vitality and spirit",
                    Image = Resources.Load<Sprite>(path + "Bloodstone_icon"),
                    ItemPrefab = PrefabDictionary[Item.Bloodstone],

                    Cost = Constants.Bloodstone.Cost,
                    AtkSpeedBonus = Constants.Bloodstone.AttackSpeed,
                    DamageBonus = Constants.Bloodstone.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Reaver",
                    Description = Constants.Reaver.ToString(), // "A massive axe capable of tearing whole mountains down",
                    Image = Resources.Load<Sprite>(path + "Reaver_icon"),
                    ItemPrefab = PrefabDictionary[Item.Reaver],

                    Cost = Constants.Reaver.Cost,
                    AtkSpeedBonus = Constants.Reaver.AttackSpeed,
                    DamageBonus = Constants.Reaver.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Divine Rapier",
                    Description = Constants.DivineRapier.ToString(), //"So powerful, it cannot have a single owner.",
                    Image = Resources.Load<Sprite>(path + "Divine_Rapier_icon"),
                    ItemPrefab = PrefabDictionary[Item.Divine],

                    Cost = Constants.DivineRapier.Cost,
                    AtkSpeedBonus = Constants.DivineRapier.AttackSpeed,
                    DamageBonus = Constants.DivineRapier.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Recipe",
                    Description = Constants.Recipe.ToString(), // "???",
                    Image = Resources.Load<Sprite>(path + "Recipe_Scroll_icon"),
                    ItemPrefab = PrefabDictionary[Item.Recipe],

                    Cost = Constants.Recipe.Cost,
                    AtkSpeedBonus = Constants.Recipe.AttackSpeed,
                    DamageBonus = Constants.Recipe.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Mjollnir",
                    Description = Constants.Mjollnir.ToString(), // "Lightening hammer thingy",
                    Image =  Resources.Load<Sprite>(path + "mjollnir_icon"),
                    Cost = Constants.Mjollnir.Cost,
                    ItemPrefab = PrefabDictionary[Item.Mjollnir],
                    AtkSpeedBonus = Constants.Mjollnir.AttackSpeed,
                    DamageBonus = Constants.Mjollnir.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Black King Bar",
                    Description = Constants.BlackKingBar.ToString(), // "Immunity",
                    Image = Resources.Load<Sprite>(path + "black_king_bar_icon"),
                    ItemPrefab = PrefabDictionary[Item.BlackKingBar],

                    Cost = Constants.BlackKingBar.Cost,
                    AtkSpeedBonus = Constants.BlackKingBar.AttackSpeed,
                    DamageBonus = Constants.BlackKingBar.Damage,
                }
            },
            {
                new ItemDto()
                {
                    Name = "Shiva's Guard",
                    Description = Constants.ShivasGuard.ToString(), // "Slow attacker",
                    Image = Resources.Load<Sprite>(path + "shivas_guard_icon"),
                    ItemPrefab = PrefabDictionary[Item.ShivasGuard],

                    Cost = Constants.ShivasGuard.Cost,
                    AtkSpeedBonus = Constants.ShivasGuard.AttackSpeed,
                    DamageBonus = Constants.ShivasGuard.Damage,
                }
            }
        };

        // Sort by cost and return
        return items.OrderBy(x => x.Cost).ToList();
    }

    protected override void SetPrefabInfo(GameObject instantiatedPrefab, ItemDto data)
    {
        base.SetPrefabInfo(instantiatedPrefab, data);

        ElementDisplay e = instantiatedPrefab.GetComponent<ElementDisplay>();
        e.SetInfo(new ElementDisplay.UIInfo()
        {
            Title = data.Name,
            Description = data.Description,
            Cost = data.Cost.ToString(),
            Sprite = data.Image,
            SpriteSize = m_imgSize,
        }, data, (i) => BuyItem(i));
    }

    private void BuyItem(ItemDto item)
    {
        if (!m_scene.CanBuy(item.Cost))
        {
            Debug.Log("Can't buy item '" + item.Name + "'");
            m_source.PlayOneShot(GlobalSoundManager.GetInvalidSound());
            return;
        }

        if (!m_playerItems.HasSpace())
        {
            m_source.PlayOneShot(GlobalSoundManager.GetInvalidSound());
            return;
        }
        
        m_scene.RemoveGold(item.Cost);
        m_source.PlayOneShot(GlobalSoundManager.GetShopBuySound());

        SceneController.GetEventManager().QueueEvent(new OnBuyItem(item));

        // Spawn 3D item and delete after X seconds
        GameObject gameItem = SpawnItem(item, m_targetSpawn);
        Destroy(gameItem, 5f);

        if(BuyCounts.ContainsKey(item.Name))
            BuyCounts[item.Name]++;
        else
            BuyCounts.Add(item.Name, 1);

        TotalItemsBought++;
    }

    private GameObject SpawnItem(ItemDto item, Transform target)
    {
        return Instantiate(item.ItemPrefab, target);
    }
}
