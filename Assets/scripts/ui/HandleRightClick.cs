﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleRightClick : MonoBehaviour
{
    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            //find the context menu
            GameObject contextMenu = GameObject.FindWithTag("context menu");
            //set the parent to this game object
            contextMenu.transform.SetParent(transform, false);
            //enable the canvas to show the menu
            contextMenu.GetComponent<Canvas>().enabled = true;
        }
    }
}