﻿using System.Collections.Generic;
using TMPro;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HeroAbilitiesList : ItemsControlBase<AbilityDto>
{
    public List<AbilityDto> BoughtAbilities { get; set; }

    [SerializeField]
    private Vector2 m_imgSize = Vector2.zero;

    [SerializeField]
    private GameObject m_soldUIAdditivePrefab = null;

    private SceneController m_scene = null;
    private EventManager m_events = null;

    private AudioSource m_source = null;

    private void Awake()
    {
        m_scene = FindObjectOfType<SceneController>();

        m_source = GetComponent<AudioSource>();
        if (m_source == null)
            m_source = this.gameObject.AddComponent<AudioSource>();
    }

    protected override void Start()
    {
        base.Start();
        m_events = SceneController.GetEventManager();
        m_events.AddListener<OnHeroBought>(OnBoughtHero);

        BoughtAbilities = new List<AbilityDto>();

        UpdateUI(GetAbilities());
    }

    protected override void SetPrefabInfo(GameObject instantiatedPrefab, AbilityDto data)
    {
        base.SetPrefabInfo(instantiatedPrefab, data);

        bool isSold = BoughtAbilities.Exists(x => x.Name == data.Name);
        bool isHeroBought = !m_scene.Heroes.Exists(x => x.IsBought && x.Hero == data.Hero);

        ElementDisplay e = instantiatedPrefab.GetComponent<ElementDisplay>();
        e.SetInfo(new ElementDisplay.UIInfo()
        {
            Title = data.Name,
            Description = data.Description,
            Cost = data.Cost.ToString(),
            Sprite = data.Image,
            SpriteSize = m_imgSize,
        }, data, (i) => BuyAbility(i));

        GameObject inst = Instantiate(m_soldUIAdditivePrefab, instantiatedPrefab.transform, false);
        Image img = Utilities.GetActualChildComponent<Image>(inst);
        TextMeshProUGUI txt = inst.GetComponentInChildren<TextMeshProUGUI>();

        if (isHeroBought)
        {
            img.sprite = Resources.Load<Sprite>("images/ui/lock_medium");
            txt.text = "Locked";
        }
        else if (isSold)
        {
            img.sprite = Resources.Load<Sprite>("images/ui/gold");
            txt.text = "Sold";
        }

        inst.SetActive(isSold || isHeroBought);
    }

    private void BuyAbility(AbilityDto ability)
    {
        if (!m_scene.CanBuy(ability.Cost))
        {
            Debug.LogError($"Can't buy ability '{ability.Name}'");
            m_source.PlayOneShot(GlobalSoundManager.GetInvalidSound());
            return;
        }

        m_scene.RemoveGold(ability.Cost);
        m_source.PlayOneShot(GlobalSoundManager.GetShopBuySound());
        m_scene.AddAbility(ability);
        BoughtAbilities.Add(ability);

        UpdateUI(GetAbilities());
    }

    private void OnBoughtHero(OnHeroBought e)
    {
        UpdateUI(GetAbilities());
    }

    private List<AbilityDto> GetAbilities()
    {
        string path = "images/ui/ability-icons/";
        List<AbilityDto> abilities = new List<AbilityDto>()
        {
            {
                new AbilityDto()
                {
                    Name = "Crystal Nova",
                    Hero = Hero.CrystalMaiden,
                    Description = "Overcharges Io to double his output for 30 seconds. Cooldown: " + 10 + " seconds",
                    Index = 0,
                    Image = Resources.Load<Sprite>(path + "crystalmaiden-1"),

                    Cost = Constants.CrystalNova.Cost,
                    BaseCooldown = Constants.CrystalNova.BaseCooldown,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Frostbite",
                    Description = "Quadruples Io's click amount for 20 seconds. Cooldown: " + 60 + " seconds",
                    Hero = Hero.CrystalMaiden,
                    Index = 1,
                    Image = Resources.Load<Sprite>(path + "crystalmaiden-2"),

                    Cost = Constants.Frostbite.Cost,
                    BaseCooldown = Constants.Frostbite.BaseCooldown,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Telekinesis",
                    Description = "Rubick lifts his click amount by 2 for 30 seconds. Cooldown: " + 60 + " seconds",
                    Hero = Hero.Rubick,
                    Index = 0,
                    Image = Resources.Load<Sprite>(path + "rubick-1"),
                    Cost = Constants.TelekinesisCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Spell Steal",
                    Description = "Rubick steals another heroes click amount for one click. Cooldown: " + 180 + " seconds",
                    Hero = Hero.Rubick,
                    Index = 1,

                    Image = Resources.Load<Sprite>(path + "rubick-2"),
                    Cost = Constants.SpellStealCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Fireblast",
                    Description = "The Ogre Magi blasts a wave of fire giving 3x his click amount for 45 seconds. Cooldown: " + 30 + " seconds",
                    Hero = Hero.OgreMagi,
                    Index = 0,

                    Image = Resources.Load<Sprite>(path + "ogremagi-1"),
                    Cost = Constants.FireblastCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Bloodlust",
                    Description = "Incites a frenzy in the Magi, decreasing his click duration by 30 seconds. Cooldown: " + 210 + " seconds",
                    Hero = Hero.OgreMagi,
                    Index = 1,

                    Image = Resources.Load<Sprite>(path + "ogremagi-2"),
                    Cost = Constants.BloodlustCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Snowball",
                    Description = "Tusk snowballs his click amount by 2. Cooldown: " + 60 + " seconds",
                    Hero = Hero.Tusk,
                    Index = 0,

                    Image = Resources.Load<Sprite>(path + "tusk-1"),
                    Cost = Constants.SnowballCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Walrus Punch",
                    Description = "Tusk connects with his mighty fist and gives you a bonus click. Cooldown: " + 120 + " seconds",
                    Hero = Hero.Tusk,
                    Index = 1,

                    Image = Resources.Load<Sprite>(path + "tusk-2"),
                    Cost = Constants.WalrusPunchCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Sunray",
                    Description = "A beam of light powerful enough to decrease all cooldowns by a minute. Cooldown: " + 60 + " seconds",
                    Hero = Hero.Phoenix,
                    Index = 0,

                    Image = Resources.Load<Sprite>(path + "phoenix-1"),
                    Cost = Constants.SunrayCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Supernova",
                    Description = "Completes a click every second of Supernova's duration. Cooldown: " + 120 + " seconds",
                    Hero = Hero.Phoenix,
                    Index = 1,

                    Image = Resources.Load<Sprite>(path + "phoenix-2"),
                    Cost = Constants.SupernovaCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "War Cry",
                    Description = "Decreases each clickers duration by 1/4. Cooldown: " + 120 + " seconds",
                    Hero = Hero.Sven,
                    Index = 0,

                    Image = Resources.Load<Sprite>(path + "sven-1"),
                    Cost = Constants.WarCryCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "God's Strength",
                    Description = "Sven channels his rogue strength, increasing his teammates click amount by 2 for 30 seconds. Cooldown: " + 210 + " seconds",
                    Hero = Hero.Sven,
                    Index = 1,

                    Image = Resources.Load<Sprite>(path + "sven-2"),
                    Cost = Constants.GodsStrengthCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Blink",
                    Description = "In a blink, Anti Mage gives you a click for free. Cooldown: " + 60 + " seconds",
                    Hero = Hero.AntiMage,
                    Index = 0,

                    Image = Resources.Load<Sprite>(path + "antimage-1"),
                    Cost = Constants.BlinkCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Mana Void",
                    Description = "For each second missing from AM's current click duration, the surrounding heroes get that duration taken off their current time. Cooldown: " + 120 + " seconds",
                    Hero = Hero.AntiMage,
                    Index = 1,

                    Image = Resources.Load<Sprite>(path + "antimage-2"),
                    Cost = Constants.ManaVoidCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Greevil's Greed",
                    Description = "For every attack, the Alchemist reduces his click duration by 20 seconds, lasts 1 minute. Cooldown: " + 210 + " seconds",
                    Hero = Hero.Alchemist,
                    Index = 0,

                    Image = Resources.Load<Sprite>(path + "alchemist-1"),
                    Cost = Constants.GreevilsGreedCost,
                }
            },
            {
                new AbilityDto()
                {
                    Name = "Chemical Rage",
                    Description = "The Alchemist causes his Ogre to enter a chemically induced rage reducing his current click by 3/4. Cooldown: " + 480 + " seconds",
                    Hero = Hero.Alchemist,
                    Index = 1,

                    Image = Resources.Load<Sprite>("Images/UI/ability-icons/alchemist-2"),
                    Cost = Constants.ChemicalRageCost,
                }
            }
        };
        return abilities;
    }
}
