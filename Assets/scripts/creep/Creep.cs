﻿using UnityEngine;

public class Creep
{
    public float CurrentHealth = 0f;
    /// <summary>
    /// Health of the creep
    /// </summary>
    public float TotalHealth = 0f;
    /// <summary>
    /// Amount of gold to earn when this creep is killed
    /// </summary>
    public int Bounty = 1;

    public float XP = 0f;

    public event System.Action<float, int> OnDeath;

    public Creep(float totalHealth = 0f)
    {
        TotalHealth = Random.Range(Constants.CreepHealth.Min, Constants.CreepHealth.Max);
        XP = Random.Range(Constants.LastHitXP.Min, Constants.LastHitXP.Max);
        Bounty = Random.Range(Constants.BaseCreepBounty.Min, Constants.BaseCreepBounty.Max);

        CurrentHealth = TotalHealth;
    }

    /// <summary>
    /// Removes health from a creep
    /// </summary>
    /// <param name="damage"></param>
    public void TakeDamage(float damage)
    {
        CurrentHealth -= damage;

        if(CurrentHealth <= 0f)
        {
            Death();
        }
    }

    private void Death()
    {
        OnDeath?.Invoke(XP, Bounty);
    }
}
