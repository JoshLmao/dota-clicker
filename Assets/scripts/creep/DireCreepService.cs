﻿using UnityEngine;

public class DireCreepService : MonoBehaviour
{
    public static GameObject[] DireCreeps;

    [SerializeField]
    GameObject[] m_localDireCreeps = null;

    private void Awake()
    {
        DireCreeps = m_localDireCreeps;
    }

    public static GameObject GetRandomDireCreepPrefab()
    {
        return DireCreeps[Random.Range(0, DireCreeps.Length - 1)];
    }
}
