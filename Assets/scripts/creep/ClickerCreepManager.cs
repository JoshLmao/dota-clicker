﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ClickerCreepManager : MonoBehaviour
{
    public event Action<float, int> OnCreepDeath;

    private Creep m_creep = null;
    private GameObject m_currentSpawnedCreep = null;

    private ClickerHeroBase m_hero = null;

    private const string IS_ATTACKING = "isAttacking";
    private const string ON_DEATH = "onDeath";
    /// <summary>
    /// Amount of seconds to 
    /// </summary>
    private const float SINK_TIME = 3f;

    public void SpawnCreep(ClickerHeroBase hero)
    {
        m_hero = hero;
        InitCreep();
    }

    private void InitCreep()
    {
        if (m_creep != null)
        {
            m_creep = new Creep();
            m_creep.OnDeath += OnDeath;
        }

        CreateCreep();
    }

    public void TakeDamage(float attackDamage)
    {
        m_creep.TakeDamage(attackDamage);
        SetPrefabCreepHealth(m_creep, m_currentSpawnedCreep);
    }

    private void SetPrefabCreepHealth(Creep creep, GameObject prefab)
    {
        Slider s = prefab.GetComponentInChildren<Slider>();
        s.maxValue = creep.TotalHealth;
        s.value = creep.CurrentHealth;
    }

    private void OnDeath(float xp, int bounty)
    {
        DestroyCreep(xp, bounty);

        CreateCreep();
    }

    private void CreateCreep()
    {
        GameObject prefab = DireCreepService.GetRandomDireCreepPrefab();
        m_currentSpawnedCreep = Instantiate(prefab, this.transform);

        m_creep = new Creep();
        m_creep.OnDeath += OnDeath;

        if (m_hero.IsAutoAttacking)
        {
            m_currentSpawnedCreep.GetComponentInChildren<Animator>().SetBool(IS_ATTACKING, true);
        }

        SetPrefabCreepHealth(m_creep, m_currentSpawnedCreep);
    }

    private void DestroyCreep(float xp, int bounty)
    {
        // Death anim with destroy
        m_currentSpawnedCreep.GetComponentInChildren<Animator>().SetTrigger(ON_DEATH);
        // Hide creep health on death
        m_currentSpawnedCreep.GetComponentInChildren<Slider>().gameObject.SetActive(false);

        StartCoroutine(SinkAndDestroy(m_currentSpawnedCreep, SINK_TIME));
        m_currentSpawnedCreep = null;

        OnCreepDeath?.Invoke(xp, bounty);

        m_creep.OnDeath -= OnDeath;
        m_creep = null;
    }

    private IEnumerator SinkAndDestroy(GameObject sinkCreep, float seconds)
    {
        // Wait for death anim and fall to floor
        yield return new WaitForSeconds(2f);

        // Sink model into the floor
        float secondsPassed = 0f;
        float sinkRate = 0.005f;
        while (secondsPassed < seconds)
        {
            var currentPos = sinkCreep.transform.localPosition;
            sinkCreep.transform.localPosition = new Vector3(currentPos.x, currentPos.y - sinkRate, currentPos.z);

            secondsPassed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        // Destroy once out of view
        Destroy(sinkCreep);
    }

    public void StartAttackOnce()
    {
        m_currentSpawnedCreep.GetComponentInChildren<Animator>().SetBool(IS_ATTACKING, true);
    }

    public void FinishAttackOnce()
    {
        m_currentSpawnedCreep.GetComponentInChildren<Animator>().SetBool(IS_ATTACKING, false);
    }
}
