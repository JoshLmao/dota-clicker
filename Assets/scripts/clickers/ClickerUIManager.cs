﻿using System.Linq;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ClickerUIManager : MonoBehaviour
{
    public Dictionary<int, GameObject> UIItemObjects { get; set; }

    public event Action<AbilityDto> OnUseAbility;

    [SerializeField]
    private GameObject m_buyParent = null;

    [SerializeField]
    private GameObject m_functionParent= null;

    [SerializeField]
    private Image m_heroImg = null;

    [SerializeField]
    private Button[] m_abilities = null;

    [SerializeField]
    private Image[] m_items = null;

    [SerializeField]
    private Slider m_xpSlider = null;

    [SerializeField]
    private TextMeshProUGUI m_level = null;

    [SerializeField]
    private Button m_buyButton = null;

    [SerializeField]
    private TextMeshProUGUI m_costText = null;

    [SerializeField]
    private GameObject m_itemHolderPrefab = null;

    [SerializeField]
    private TextMeshProUGUI m_attackDmgText = null;

    [SerializeField]
    private TextMeshProUGUI m_attackSpdText = null;

    [SerializeField]
    private GameObject m_requiredHeroesParent;

    [SerializeField]
    private Vector2 m_reqHeroImgSize = Vector2.one;

    private ClickerHeroBase m_hero = null;
    private int m_buyCost = 0;
    private EventManager m_events = null;

    public void Awake()
    {
        if (m_buyButton != null)
            m_buyButton.onClick.AddListener(OnBuyHero);

        UIItemObjects = new Dictionary<int, GameObject>();
    }

    public void Start()
    {
        m_events = SceneController.GetEventManager();

        Camera c = Camera.allCameras[0];
        m_buyButton.transform.parent.GetComponent<Canvas>().worldCamera = c;
        m_heroImg.transform.parent.GetComponent<Canvas>().worldCamera = c;

        InitItems();

        // Configure ability cooldown UI
        for (int i = 0; i < m_abilities.Length; i++)
        {
            int num = i;

            Image fadeImg = Utilities.GetActualChildComponent<Image>(m_abilities[i].gameObject);
            fadeImg.fillAmount = 0;
            fadeImg.color = new Color(fadeImg.color.r, fadeImg.color.g, fadeImg.color.b, 0.9f);

            TextMeshProUGUI cooldownText = m_abilities[i].GetComponentInChildren<TextMeshProUGUI>();
            cooldownText.text = "";

            m_abilities[i].interactable = m_hero.Abilities.Exists(x => x.Index == i);
            m_abilities[i].onClick.AddListener(() => OnAbilityClicked(num));
        }

        Canvas[] cs = GetComponentsInChildren<Canvas>(true);
        foreach(Canvas canvas in cs)
            canvas.worldCamera = Camera.allCameras[0];
    }

    private void InitItems()
    {
        for (int i = 0; i < m_items.Length; i++)
        {
            //Store the current num so it get's sent correctly through the events (C# bug?)
            int num = i;

            ClickPropagator click = m_items[i].gameObject.AddComponent<ClickPropagator>();
            click.MouseUp += (obj) => OnItemMouseUp(obj, num);
            click.MouseEnter += (obj) => OnItemMouseEnter(obj, num);
            click.MouseExit += (obj) => OnItemMouseExit(obj, num);
            click.RightMouseUp += (obj) => OnRightMouseUp(obj, num);
        }
    }

    public void SetHero(ClickerHeroBase hero, int buyCost, List<AbilityDto> abilities, double attackDmg, double attackTime)
    {
        m_hero = hero;
        m_buyCost = buyCost;

        UpdateUI(abilities, attackDmg, attackTime);
    }

    public void UpdateUI(List<AbilityDto> abilities, double attackDmg, double attackTime)
    {
        LoadHeroImage(m_hero.Hero);
        LoadAbilityIcons(abilities);

        m_costText.text = m_buyCost.ToString();
        UpdateDamage(attackDmg);
        UpdateAttackSpeed(attackTime);

        UpdateXPBar(0, GetComponentInParent<ClickerHeroBase>().LevelUpAmount, 1);
    }

    private void LoadHeroImage(Hero hero)
    {
        string imgPath = $"images/ui/hero-icons/{hero.ToString()}";
        LoadSetImage(imgPath, m_heroImg);
    }

    private void LoadAbilityIcons(List<AbilityDto> abilities)
    {
        for (int i = 0; i < m_abilities.Length; i++)
        {
            m_abilities[i].interactable = abilities.Exists(x => x.Index == i);

            Image img = m_abilities[i].GetComponent<Image>();
            AbilityDto ability = abilities.FirstOrDefault(x => x.Index == i);
            LoadSetImage($"images/ui/ability-icons/{m_hero.Hero.ToString()}-{i + 1}", img);

            // Set cool down fade
            Image fadeImg = m_abilities[i].GetComponentInChildren<Image>();
            Color color = fadeImg.color;
            fadeImg.color = new Color(color.r, color.g, color.b, ability != null ? 1f : 0.25f);
        }
    }

    private void LoadSetImage(string path, Image img)
    {
        Sprite s = Resources.Load<Sprite>(path);
        if (s == null)
            Debug.LogError($"Unable to load sprite at path '{path}'");
        else
            img.sprite = s;
    }

    public void UpdateDamage(double dmg)
    {
        m_attackDmgText.text = dmg.ToString();
    }

    public void UpdateAttackSpeed(double attackTime)
    {
        m_attackSpdText.text = attackTime.ToString();
    }

    /// <summary>
    /// Updates the UI for the level
    /// </summary>
    /// <param name="xp"></param>
    /// <param name="maxXp"></param>
    /// <param name="level"></param>
    public void UpdateXPBar(float xp, float maxXp, int level)
    {
        m_level.text = level.ToString();

        m_xpSlider.maxValue = maxXp;
        m_xpSlider.value = xp;
    }

    public void SetBought(bool isBought)
    {
        m_buyParent.SetActive(!isBought);
        m_functionParent.SetActive(isBought);
    }

    private void OnBuyHero()
    {
        bool hasBoughtHero = FindObjectOfType<SceneController>().BuyHero(GetComponentInParent<ClickerHeroBase>(), m_hero.Hero, m_buyCost);
    }

    /// <summary>
    /// Updates the UI for the abilities
    /// </summary>
    /// <param name="abilities"></param>
    public void UpdateAbilities(List<AbilityDto> abilities)
    {
        LoadAbilityIcons(abilities);
    }

    public void UpdateInventory(Dictionary<int, ItemDto> items)
    {
        // Destroy and clear the old Inventory
        GameObject[] toDestroy = UIItemObjects.Select(x => x.Value).ToArray();
        foreach (GameObject obj in toDestroy)
            Destroy(obj);
        UIItemObjects.Clear();

        for (int i = 0; i < m_items.Length; i++)
        {
            if (items.ContainsKey(i))
            {
                GameObject inst = Instantiate(m_itemHolderPrefab, m_items[i].gameObject.transform);
                inst.GetComponent<Image>().sprite = items[i].Image;

                UIItemObjects.Add(i, inst);
            }
            else
            {
                if(UIItemObjects.ContainsKey(i))
                {
                    Destroy(UIItemObjects[i]);
                    UIItemObjects.Remove(i);
                }
            }
        }
    }

    private void OnItemMouseEnter(GameObject obj, int slotIndex)
    {
        m_events.TriggerEvent(new OnMouseEnterHeroInvSlot(slotIndex));
    }

    private void OnItemMouseExit(GameObject obj, int slotIndex)
    {
        m_events.TriggerEvent(new OnMouseExitHeroInvSlot(slotIndex));
    }

    private void OnItemMouseUp(GameObject mouseUpObj, int slotIndex)
    {
        m_events.TriggerEvent(new OnMouseUpHeroInvSlot(m_hero, slotIndex));
    }

    private void OnRightMouseUp(GameObject rightMouseUpObj, int slotIndex)
    {
        if (m_hero.Items.ContainsKey(slotIndex))
        {
            GameObject obj = Resources.Load<GameObject>("Prefabs/UI/ContextMenu-WorldSpace");
            GameObject inst = Instantiate(obj);
            inst.transform.SetParent(rightMouseUpObj.transform, false);
            inst.SetActive(true);

            float scale = 3.0f;
            inst.transform.localScale = new Vector3(scale, scale, scale);
            var rect = inst.GetComponent<RectTransform>();
            inst.transform.localPosition = new Vector3(rect.rect.width / 2 * scale, rect.rect.height / 2 * scale);

            // Configure the world space context menu
            WorldSpaceContextMenu ws = inst.GetComponent<WorldSpaceContextMenu>();
            ws.Configure(m_hero.Items[slotIndex], slotIndex, m_hero.Hero);
        }
    }

    public void UpdateCourier(ClickerCourierDto courier)
    {
        /// ToDo
    }

    /// <summary>
    /// Updates the UI with the require heroes to buy
    /// </summary>
    /// <param name="requiredHeroes"></param>
    public void SetRequiredHeroes(List<Hero> requiredHeroes)
    {
        var ts = m_requiredHeroesParent.GetComponentsInChildren<Transform>();
        foreach(var t in ts)
        {
            if (t != m_requiredHeroesParent.transform)
                Destroy(t.gameObject);
        }

        int imgIndex = 0;
        float spacing = 5f;
        foreach (Hero h in requiredHeroes)
        {
            GameObject obj = new GameObject($"ReqHero-{h}");
            Image img = obj.AddComponent<Image>();
            img.sprite = Resources.Load<Sprite>($"images/ui/hero-icons/{h}");

            // Set size, height then width
            RectTransform rect = obj.GetComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 0.5f);
            rect.anchorMax = new Vector2(0, 0.5f);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, m_reqHeroImgSize.y);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_reqHeroImgSize.x);

            obj.transform.SetParent(m_requiredHeroesParent.transform);
            obj.transform.localScale = Vector3.one;
            obj.transform.localRotation = Quaternion.identity;
            float xPos = (rect.rect.width / 2) + (rect.rect.width * imgIndex) + (imgIndex > 0 ? spacing * imgIndex : 0f);
            rect.localPosition = Vector3.zero;
            rect.anchoredPosition = new Vector3(xPos, 0f, 0f);
            
            imgIndex++;
        }

        RectTransform parentRect = m_requiredHeroesParent.GetComponent<RectTransform>();
        float totalWidth = (m_reqHeroImgSize.x * imgIndex) + (spacing * imgIndex);
        parentRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, totalWidth);
    }

    public void UseAbility(int index, float cooldownSeconds)
    {
        StartCoroutine(AbilityCooldownRoutine(m_abilities[index], cooldownSeconds));
    }

    private System.Collections.IEnumerator AbilityCooldownRoutine(Button abilityBtn, float cdSeconds)
    {
        float remainingSeconds = cdSeconds;
        Image cdImg = Utilities.GetActualChildComponent<Image>(abilityBtn.gameObject);
        TextMeshProUGUI cdText = abilityBtn.GetComponentInChildren<TextMeshProUGUI>();

        while (remainingSeconds > 0f)
        {
            float time = Mathf.Round(remainingSeconds);
            cdText.text = (time + 1).ToString();

            float scaledValue = remainingSeconds / cdSeconds;
            cdImg.fillAmount = scaledValue;

            remainingSeconds -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        cdText.text = "";
    }

    private void OnAbilityClicked(int abilityIndex)
    {
        bool hasAbility = m_hero.Abilities.Exists(x => x.Index == abilityIndex);
        if (hasAbility)
            OnUseAbility?.Invoke(m_hero.Abilities.FirstOrDefault(x => x.Index == abilityIndex));
    }
}
