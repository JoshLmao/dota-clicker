﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickerResponseManager : MonoBehaviour
{
    private Dictionary<int, AudioClip> m_abilitySounds = null;
    private AudioClip[] m_atkResponses = null;

    private AudioSource m_source = null;
    private bool m_isCoolingDown = false;

    /// <summary>
    /// Amount of seconds inbetween each response
    /// </summary>
    private const float SOUND_COOLDOWN_SECONDS = 5f;

    private void Start()
    {
        m_source = this.gameObject.AddComponent<AudioSource>();
    }

    public void SetAttackResponse(AudioClip[] atkResponses)
    {
        m_atkResponses = atkResponses;
    }

    public void SetAbilitySounds(Dictionary<int, AudioClip> abilitySounds)
    {
        m_abilitySounds = abilitySounds;
    }

    public void PlayAttackSound()
    {
        if (!m_isCoolingDown)
        {
            Play(GetRandomClip(m_atkResponses));
            StartCoroutine(DoCooldown(SOUND_COOLDOWN_SECONDS));
        }
    }

    public void PlayAbiltySound(AbilityDto ability)
    {
        if (!m_isCoolingDown)
        {
            Play(m_abilitySounds[ability.Index]);
            StartCoroutine(DoCooldown(SOUND_COOLDOWN_SECONDS));
        }
    }

    private void Play(AudioClip clip)
    {
        if(!m_source.isPlaying && !m_isCoolingDown)
        {
            m_source.PlayOneShot(clip);
        }
    }

    private AudioClip GetRandomClip(AudioClip[] clips)
    {
        return clips[UnityEngine.Random.Range(0, clips.Length - 1)];
    }

    private IEnumerator DoCooldown(float seconds)
    {
        m_isCoolingDown = true;
        yield return new WaitForSeconds(seconds);
        m_isCoolingDown = false;
    }
}
