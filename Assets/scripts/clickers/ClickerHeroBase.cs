﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ClickerHeroBase : MonoBehaviour
{
    /// <summary>
    /// The display name of the hero
    /// </summary>
    public Hero Hero;
    /// <summary>
    /// Base attack damage the hero does
    /// </summary>
    public double BaseAttackDamage = 1.0;
    /// <summary>
    /// Time in seconds it takes to complete one attack
    /// </summary>
    public double BaseAttackTime = 1.0;

    /// <summary>
    /// The base amount for earning a last hit
    /// </summary>
    public int BaseLastHitMultiplier = 1;
    /// <summary>
    /// Current XP of hero
    /// </summary>
    [HideInInspector]
    public float HeroXP = 0f;
    /// <summary>
    /// Current level of the hero
    /// </summary>
    [HideInInspector]
    public int Level = 1;
    /// <summary>
    /// Amount of gold to buy this hero
    /// </summary>
    public int BuyAmount = 100;
    /// <summary>
    /// Is the hero bought and can be used as a clicker
    /// </summary>
    [HideInInspector]
    public bool IsBought = false;

    /// <summary>
    /// All heroes required before the current hero can be bought
    /// </summary>
    public RequiredHeroDict RequiredHeroes = null;

    /// <summary>
    /// List of abilities the hero has available
    /// </summary>
    public List<AbilityDto> Abilities { get; set; }
    /// <summary>
    /// All items the hero has (item slot index, item)
    /// </summary>
    public Dictionary<int, ItemDto> Items { get; set; }
    /// <summary>
    /// Active courier for this hero
    /// </summary>
    public ClickerCourierDto Courier { get; private set; }

    /// <summary>
    /// UI manager for the hero
    /// </summary>
    [HideInInspector]
    public ClickerUIManager ClickerUI = null;

    protected virtual AudioClip[] AttackingResponses { get; }
    protected virtual Dictionary<int, AudioClip> AbilitySounds { get; }

    /// <summary>
    /// The prefab of the model
    /// </summary>
    [SerializeField]
    private GameObject m_modelPrefab = null;
    /// <summary>
    /// Prefab for the UI
    /// </summary>
    [SerializeField]
    private GameObject m_displayPrefab = null;
    /// <summary>
    /// Spawn position of the model
    /// </summary>
    [SerializeField]
    private Transform m_modelSpawnPos = null;
    /// <summary>
    /// Spawn position for the UI
    /// </summary>
    [SerializeField]
    private Transform m_displaySpawnPos = null;
    [SerializeField]
    private Transform m_creepPosition = null;

    [SerializeField]
    private Transform m_courierParent = null;

    /// <summary>
    /// The xp needed to level up
    /// </summary>
    public float LevelUpAmount { get { return Constants.BASE_XP_AMOUNT * Level; } }

    public bool IsAutoAttacking { get { return Courier != null; } }

    private SceneController m_scene;

    protected GameObject m_model;
    /// <summary>
    /// Model controller for setting hero model states
    /// </summary>
    protected HeroModelController m_modelController = null;
    protected CourierManager m_courierManager = null;
    protected ClickerCreepManager m_creepManager = null;
    protected ClickerResponseManager m_responseManager = null;

    private bool m_isAttackOnce = false;
    private Dictionary<AbilityDto, DateTime> m_cooldownAbilities = new Dictionary<AbilityDto, DateTime>();


    #region Mono Behavious
    protected virtual void Awake()
    {
        m_scene = FindObjectOfType<SceneController>();

        Abilities = new List<AbilityDto>();
        Items = new Dictionary<int, ItemDto>();
        m_courierManager = this.gameObject.AddComponent<CourierManager>();

        IsBought = Hero == Hero.CrystalMaiden;
    }

    protected virtual void Start()
    {
        EventManager events = SceneController.GetEventManager();
        events.AddListener<HeroMouseUp>(OnHeroMouseUp);
        events.AddListener<OnHeroBought>(HeroBought);

        // Setup model
        if (m_modelSpawnPos != null && m_modelPrefab != null)
        {
            m_model = Instantiate(m_modelPrefab, m_modelSpawnPos);
            m_modelController = Utilities.GetAddComponent<HeroModelController>(m_model);

            ConfigureModel();
        }

        // Setup UI display
        if (m_displayPrefab != null && m_displaySpawnPos != null)
        {
            m_displayPrefab = Instantiate(m_displayPrefab, m_displaySpawnPos);
            ClickerUI = Utilities.GetAddComponent<ClickerUIManager>(m_displayPrefab);
            ClickerUI.OnUseAbility += OnAbilityClicked;

            ConfigureClickerUI();
        }

        // Setup creep manager
        if(m_creepPosition != null)
        {
            m_creepManager = m_creepPosition.gameObject.AddComponent<ClickerCreepManager>();
            m_creepManager.OnCreepDeath += OnCreepDeath;
        }

        GameObject obj = new GameObject("ResponseManager");
        obj.transform.SetParent(this.transform);
        obj.transform.localPosition = Vector3.zero;
        m_responseManager = obj.AddComponent<ClickerResponseManager>();
        m_responseManager.SetAttackResponse(AttackingResponses);
        m_responseManager.SetAbilitySounds(AbilitySounds);

        m_modelController.SetBought(IsBought);
        ClickerUI.SetBought(IsBought);
        if (IsBought)
        {
            if (m_creepManager != null)
                m_creepManager.SpawnCreep(this);

            if(Courier != null)
                StartAutoAttacking();
        }

        ClickerUI.SetRequiredHeroes(RequiredHeroes.Values.ToList());
    }

    protected virtual void Update()
    {

    }

    private void OnMouseDown()
    {
        DoOneAttack();
    }
    #endregion

    private void ConfigureClickerUI()
    {
        ClickerUI.SetHero(this, BuyAmount, Abilities, GetDamage(), GetAttackTime());
    }

    private void ConfigureModel()
    {
        m_modelController.AttackComplete += OnCompletedAttack;
        m_modelController.Hero = this;
    }

    private void OnCompletedAttack()
    {
        double atkDmg = GetDamage();
        m_creepManager.TakeDamage((float)atkDmg);

        if (m_isAttackOnce)
            FinshAttackOnce();
    }

    private void DoOneAttack()
    {
        if (IsBought)
        {
            m_isAttackOnce = true;
            m_modelController.DoAttack();
            m_creepManager.StartAttackOnce();
            m_responseManager.PlayAttackSound();
        }
    }

    private void FinshAttackOnce()
    {
        m_creepManager.FinishAttackOnce();
        m_isAttackOnce = false;
    }

    private void OnCreepDeath(float xp, int bounty)
    {
        m_scene.AddGold(bounty * BaseLastHitMultiplier);
        AddXP(xp);
    }

    private void StartAutoAttacking()
    {
        m_modelController.AutoAttack();
    }

    private void AddXP(float xpAmount)
    {
        HeroXP += xpAmount;
        if(HeroXP >= LevelUpAmount && Level < Constants.MAX_LEVEL)
        {
            Level += 1;
            HeroXP = 0f;
        }

        ClickerUI.UpdateXPBar(HeroXP, LevelUpAmount, Level);
        ClickerUI.UpdateDamage(GetDamage());
        ClickerUI.UpdateAttackSpeed(GetAttackTime());
    }

    public void BuyHero()
    {
        if (!IsBought)
        {
            Debug.Log($"Bought hero '{Hero}'");

            IsBought = true;
            if(ClickerUI != null)
                ClickerUI.SetBought(IsBought);
            if(m_modelController != null)
                m_modelController.SetBought(IsBought);
            if(m_creepManager != null)
            m_creepManager.SpawnCreep(this);
        }
    }

    public void AddAbility(AbilityDto ability)
    {
        if(!Abilities.Contains(ability))
            Abilities.Add(ability);

        ClickerUI.UpdateAbilities(Abilities);
    }

    public void SetClickerCourier(ClickerCourierDto courier)
    {
        if(Courier != null)
        {
            // Return gold for "selling" preview courier
            m_scene.AddGold(Courier.Cost / 2);
        }

        Courier = courier;

        if (Courier != null)
        {
            m_courierManager.SetCourier(Courier, m_courierParent);
            StartAutoAttacking();
        }

        ClickerUI.UpdateCourier(Courier);
        ClickerUI.UpdateDamage(GetDamage());
        ClickerUI.UpdateAttackSpeed(GetAttackTime());
    }

    public void AddItem(int slotIndex, ItemDto item)
    {
        if (Items.ContainsKey(slotIndex))
        {
            Debug.LogError("Can't place item where an item already is!");
        }
        else
        {
            Items.Add(slotIndex, item);
        }

        ClickerUI.UpdateInventory(Items);
        ClickerUI.UpdateDamage(GetDamage());
        ClickerUI.UpdateAttackSpeed(GetAttackTime());
    }

    public void RemoveItem(int slot)
    {
        if (Items.ContainsKey(slot))
        {
            Items.Remove(slot);
        }

        ClickerUI.UpdateInventory(Items);
        ClickerUI.UpdateDamage(GetDamage());
        ClickerUI.UpdateAttackSpeed(GetAttackTime());
    }

    private void OnHeroMouseUp(HeroMouseUp e)
    {
        if (IsAutoAttacking)
            return;

        if (e.Hero == Hero)
        {
            DoOneAttack();
        }
    }

    /// <summary>
    /// Gets the time it takes to complete one attack in seconds
    /// </summary>
    /// <returns></returns>
    public double GetAttackTime()
    {
        double increasedAtkSpd = GetIncreasedAttackSpeed();
        double atkTime = BaseAttackTime - increasedAtkSpd;
        // Make sure atkTime isn't below certain threshold
        return atkTime <= 0.0 ? 0.1 : atkTime;
    }

    private double GetIncreasedAttackSpeed()
    {
        IEnumerable<double> speeds = Items.Select(x => x.Value.AtkSpeedBonus);
        // Atk bonus from items
        double amount = speeds.Sum(x => x);

        // Bonus atk speed from level
        double lvlBonus = Level > 1 ? Level / 100 : 0.0;

        // Bonus attack speed from courier
        double crowBonus = Courier != null ? Courier.AtkSpeedBonus : 0.0;

        // Add all bonuses together and return
        return amount + lvlBonus + crowBonus;
    }

    private double GetDamage()
    {
        IEnumerable<double> speeds = Items.Select(x => x.Value.DamageBonus);
        // Damage bonus from items
        double amount = speeds.Sum(x => x);

        // Bonus damage from level
        // Get percentage of base damage and add it per level
        double lvlBonus = Level > 1 ? Utilities.GetPercentOfVal(BaseAttackDamage, Constants.PercentBaseDamagePerLevel) * (Level - 1) : 0.0;

        // Bonus damage from courier
        double crowBonus = Courier != null ? Courier.DamageBonus : 0.0;

        return BaseAttackDamage + lvlBonus + crowBonus;
    }

    /// <summary>
    /// Check to see if this hero can be bought and has no pre-requisite
    /// </summary>
    /// <returns></returns>
    public bool CanBuy()
    {
        return RequiredHeroes == null || (RequiredHeroes != null && RequiredHeroes.Count == 0);
    }

    private void HeroBought(OnHeroBought e)
    {
        if (RequiredHeroes == null)
            return;

        if(RequiredHeroes.ContainsValue(e.BoughtHero))
        {
            var kvp = RequiredHeroes.FirstOrDefault(x => x.Value == e.BoughtHero);
            RequiredHeroes.Remove(kvp.Key);

            ClickerUI.SetRequiredHeroes(RequiredHeroes.Values.ToList());
        }
    }

    private void OnAbilityClicked(AbilityDto ability)
    {
        if (IsAbilityOnCooldown(ability))
            return;

        // Start cooldown straight away
        StartCoroutine(AbilityCooldown(ability, ability.BaseCooldown));

        // Apply effects, set UI, play ability sound
        ApplyAbility(ability);
        ClickerUI.UseAbility(ability.Index, ability.BaseCooldown);
        m_responseManager.PlayAbiltySound(ability);
    }

    private bool IsAbilityOnCooldown(AbilityDto ability)
    {
        return m_cooldownAbilities.ContainsKey(ability);
    }

    private System.Collections.IEnumerator AbilityCooldown(AbilityDto abil, float seconds)
    {
        m_cooldownAbilities.Add(abil, DateTime.Now);
        yield return new WaitForSeconds(seconds);
        m_cooldownAbilities.Remove(abil);
    }

    protected virtual void ApplyAbility(AbilityDto ability)
    {
        return;
    }
}
