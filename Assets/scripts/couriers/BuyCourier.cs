﻿using UnityEngine;

public class BuyCourier : MonoBehaviour
{
    public event System.Action<ClickerCourierDto, Hero, GameObject> OnSetToHero;

    public ClickerCourierDto Courier;

    private void OnMouseDown()
    {
        OnSetToHero?.Invoke(Courier, Hero.CrystalMaiden, this.gameObject);
    }
}
