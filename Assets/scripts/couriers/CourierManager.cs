﻿using UnityEngine;

public class CourierManager : MonoBehaviour
{
    public ClickerCourierDto Courier { get; private set; }

    private Transform m_courierParent;

    private GameObject m_currentCourier = null;

    private CourierService m_service;

    private void Start()
    {
        m_service = FindObjectOfType<CourierService>();
    }

    public void SetCourier(ClickerCourierDto courier, Transform spawnPosParent)
    {
        if(m_currentCourier != null)
        {
            Destroy(m_currentCourier);
        }

        m_currentCourier = Instantiate(m_service.GetCourierPrefab(courier.Courier), spawnPosParent);
    }
}
