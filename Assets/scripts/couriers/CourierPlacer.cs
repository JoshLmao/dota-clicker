﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CourierPlacer : MonoBehaviour
{
    [SerializeField]
    CourierService m_service;

    [SerializeField]
    private Transform m_spawnPosParent;

    [SerializeField]
    private GameObject m_masterParent;

    [SerializeField]
    private GameObject m_btnParent;

    [SerializeField]
    private GameObject m_heroBtnPrefab;

    [SerializeField]
    private Button m_confirmBtn;

    [SerializeField]
    private Button m_sellBtn;

    [SerializeField]
    private float m_ySpacing = 5f;

    private List<Toggle> m_heroToggles = new List<Toggle>();

    private SceneController m_scene;
    private GameObject m_previewCrow;
    private ClickerCourierDto m_boughtCrow;
    private Hero m_setHero;
    private AudioSource m_source;

    private void Awake()
    {
        m_scene = FindObjectOfType<SceneController>();

        m_source = Utilities.GetAddComponent<AudioSource>(this.gameObject);

        m_service.OnBoughtCourier += OnCourierBought;

        m_confirmBtn.onClick.AddListener(OnConfirmSet);
        m_sellBtn.onClick.AddListener(OnSellCourier);
    }

    private void Start()
    {
        EventManager events = SceneController.GetEventManager();
        events.AddListener<OnHeroBought>(OnBoughtHero);

        m_confirmBtn.interactable = false;
        m_masterParent.SetActive(false);

        SetUI(m_scene.Heroes);
    }

    public bool HasCourierWaitingSelection()
    {
        return m_previewCrow != null;
    }

    private void OnCourierBought(ClickerCourierDto courier)
    {
        if (m_previewCrow != null)
        {
            Destroy(m_previewCrow);
        }

        m_boughtCrow = courier;

        GameObject prefab = m_service.GetCourierPrefab(courier.Courier);
        m_previewCrow = Instantiate(prefab, m_spawnPosParent);

        m_masterParent.SetActive(true);

        m_sellBtn.GetComponentInChildren<TextMeshProUGUI>().text = $"Sell ({GetSellCost(m_boughtCrow.Cost)})";
    }

    private void OnBoughtHero(OnHeroBought boughtHero)
    {
        SetUI(m_scene.Heroes);
    }

    private void SetUI(List<ClickerHeroBase> heroes)
    {
        Transform[] ts = m_btnParent.GetComponentsInChildren<Transform>();
        foreach (Transform t in ts)
            if(t != m_btnParent.transform)
                Destroy(t.gameObject);

        if (heroes == null)
            return;

        ToggleGroup group = m_btnParent.GetComponent<ToggleGroup>();
        m_heroToggles.Clear();

        float yPos = -m_btnParent.GetComponent<RectTransform>().rect.y;
        yPos -= m_ySpacing;
        foreach(ClickerHeroBase hero in heroes)
        {
            GameObject btnObj = Instantiate(m_heroBtnPrefab, m_btnParent.transform);
            btnObj.transform.localPosition = new Vector3(btnObj.transform.localPosition.x, yPos, btnObj.transform.localPosition.z);

            Toggle t = btnObj.GetComponent<Toggle>();
            t.interactable = hero.IsBought;
            t.group = group;
            t.isOn = false;

            btnObj.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = hero.Hero.ToString();

            m_heroToggles.Add(t);

            Hero h = hero.Hero;
            t.onValueChanged.AddListener((state) => OnSetTargetHero(state, (Hero)h));

            yPos -= btnObj.GetComponent<RectTransform>().rect.height + m_ySpacing;
        }
    }

    private void OnSetTargetHero(bool isChecked, Hero hero)
    {
        if (isChecked)
        {
            m_setHero = hero;
            m_confirmBtn.interactable = true;
        }
        else
        {
            m_setHero = Hero.Unknown;
            m_confirmBtn.interactable = false;
        }
    }

    private void OnConfirmSet()
    {
        if (m_previewCrow != null && m_setHero != Hero.Unknown)
        {
            // Destroy preview model at shop
            Destroy(m_previewCrow);

            m_confirmBtn.interactable = false;
            m_masterParent.SetActive(false);

            // Set courier on hero
            m_service.SetToHero(m_boughtCrow, m_setHero);

            // Reset courier & toggles when done
            m_boughtCrow = null;
            m_setHero = Hero.Unknown;
            foreach (Toggle t in m_heroToggles)
                t.isOn = false;
        }
        else
        {
            Debug.LogError("Unable to set courier to hero, no courier spawned");
        }
    }

    private void OnSellCourier()
    {
        m_scene.AddGold(GetSellCost(m_boughtCrow.Cost));
        m_source.PlayOneShot(GlobalSoundManager.GetShopBuySound());

        Destroy(m_previewCrow);

        m_confirmBtn.interactable = false;
        m_masterParent.SetActive(false);

        m_boughtCrow = null;
        m_setHero = Hero.Unknown;
    }

    private int GetSellCost(int originalCost)
    {
        return originalCost - 10;
    }
}
