﻿using Newtonsoft.Json;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    protected ConfigDto m_currentConfig;

    protected Slider m_fieldOfViewSlider;
    protected float m_currentFieldOfView = 60;

    protected virtual void Start()
    {
        CreateConfig();
    }

    public void OnApplicationQuit()
    {
        SaveManager.SaveSaveFile();
        SaveCurrentValuesToConfig();
    }

    protected void CreateConfig()
    {
        if(m_currentConfig == null)
            m_currentConfig = new ConfigDto();

        ValidateSave();

        string configLoc = SaveManager.FILE_PATHS + SaveManager.CONFIG_FILE;
        string json = File.ReadAllText(configLoc);
        if (json.Length == 0)
        {
            //ToDo: Set default values for new config
            m_currentConfig.Preferences = new PreferencesDto();
        }
        else
        {
            m_currentConfig = JsonConvert.DeserializeObject<ConfigDto>(json);
        }

        string toJson = JsonConvert.SerializeObject(m_currentConfig, Formatting.Indented);
        File.WriteAllText(configLoc, toJson);
    }

    protected void ValidateSave()
    {
        if (!Directory.Exists(SaveManager.FILE_PATHS))
            Directory.CreateDirectory(SaveManager.FILE_PATHS);

        string configLoc = SaveManager.FILE_PATHS + SaveManager.CONFIG_FILE;
        if (!File.Exists(configLoc))
        {
            File.Create(configLoc).Close();
        }
    }

    protected void SaveCurrentValuesToConfig()
    {
        if (m_currentConfig == null)
            return;

        string configLoc = SaveManager.FILE_PATHS + SaveManager.CONFIG_FILE;
        m_currentConfig.Preferences = new PreferencesDto()
        {
            //ToDo: save current values to config
        };

        string toJson = JsonConvert.SerializeObject(m_currentConfig, Formatting.Indented);
        File.WriteAllText(configLoc, toJson);
    }

}
