﻿public interface IAttackSpeedSupplier
{
    /// <summary>
    /// Amount of attack speed the reciever gets
    /// </summary>
    double AtkSpeedBonus { get; set; }
}
