﻿public interface IDamageSupplier
{
    /// <summary>
    /// Amount of bonus damage the reciever gets
    /// </summary>
    double DamageBonus { get; set; }
}
