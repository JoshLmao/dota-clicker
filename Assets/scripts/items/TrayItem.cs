﻿using UnityEngine;
using UnityEngine.EventSystems;

public class TrayItem : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public ItemDto Item { get; set; }
    public int Slot = -1;

    private EventManager m_events;

    public void Start()
    {
        m_events = SceneController.GetEventManager();
    }

    public void OnMouseDown()
    {
    }

    public void OnMouseUp()
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            m_events.TriggerEvent(new OnPlayerInvItemMouseDown(Item));
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            m_events.TriggerEvent(new OnPlayerInvItemMouseUp(Slot, Item));
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (Item == null)
                return;

            GameObject obj = Resources.Load<GameObject>("Prefabs/UI/ContextMenu-WorldSpace");
            GameObject inst = Instantiate(obj);
            inst.transform.SetParent(this.gameObject.transform.parent.transform, false);
            inst.SetActive(true);

            inst.transform.localScale = Vector3.one;
            var rect = inst.GetComponent<RectTransform>();
            inst.transform.localPosition = new Vector3(rect.rect.width / 2, rect.rect.height / 2);

            // Configure the world space context menu
            WorldSpaceContextMenu ws = inst.GetComponent<WorldSpaceContextMenu>();
            ws.Configure(Item, Slot, Hero.Unknown);
        }
    }
}
