﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;

public class PlayerItemsTray : MonoBehaviour
{
    class SelectedItemsInfo
    {
        public ItemDto Item { get; private set; }
        public int Index { get; private set; }
        public ClickerHeroBase Hero { get; private set; }

        public SelectedItemsInfo(ItemDto itemDto, int targetSlot, ClickerHeroBase hero)
        {
            Item = itemDto;
            Index = targetSlot;
            Hero = hero;
        }
    }

    public Dictionary<int, ItemDto> Inventory { get; private set; }

    [SerializeField]
    private GameObject[] m_itemSlots = null;

    [SerializeField]
    private GameObject m_instantiatedItemPrefab = null;

    [SerializeField]
    private Button m_showHideBtn = null;

    [SerializeField]
    private Shader m_outlineShader = null;

    private bool m_isOpen = false;

    private SelectedItemsInfo m_selectedItemInfo;

    private void Awake()
    {
        m_showHideBtn.onClick.AddListener(OnToggleTray);
        Inventory = new Dictionary<int, ItemDto>();
    }

    private void Start()
    {
        EventManager events = SceneController.GetEventManager();
        events.AddListener<OnBuyItem>(OnBoughtItem);

        events.AddListener<OnPlayerInvItemMouseUp>(OnPlayerInvMouseUp);
        events.AddListener<OnMouseUpHeroInvSlot>(OnHeroInvSlotMouseUp);

        m_isOpen = true;
        OnToggleTray();

        UpdateInventory();
    }

    public void OnToggleTray()
    {
        m_isOpen = !m_isOpen;

        m_showHideBtn.GetComponentInChildren<TextMeshProUGUI>().text = m_isOpen ? "Hide" : "Show";
        RectTransform rectTransform = GetComponent<RectTransform>();
        float hideHeight = -rectTransform.rect.height;
        this.transform.position = new Vector3(this.transform.position.x, m_isOpen ? 0 : hideHeight, this.transform.position.z);
    }

    private void OnBoughtItem(OnBuyItem itemEvent)
    {
        int orderIndex = 0;
        int keysCount = Inventory.Keys.Count;
        for (int i = 0; i < keysCount; i++)
        {
            int key = Inventory.Keys.Contains(i) ? Inventory.Keys.FirstOrDefault(x => x == i) : -1;
            if (key == orderIndex)
                orderIndex++;
            else
                break;
        }

        if(keysCount > 0 && orderIndex == m_itemSlots.Length)
        {
            // Full inventory since we went thoguh all the keys and they match
            Debug.Log("Inventory full");
        }
        else if(keysCount == 0 || orderIndex < (m_itemSlots.Length) && orderIndex >= 0)
        {
            // Slot free, place here
            Inventory.Add(orderIndex, itemEvent.Item);
        }
        else
        {
            // Nothing, value is -1
            Debug.LogError("Unable to set to inventory space = Index is -1");
        }

        UpdateInventory();
    }

    /// <summary>
    /// Does the inventory have space to add to it
    /// </summary>
    /// <returns></returns>
    public bool HasSpace()
    {
        return Inventory.Count < m_itemSlots.Length;
    }

    private void UpdateInventory()
    {
        //Destroy all item children
        foreach(GameObject obj in m_itemSlots)
        {
            Transform[] children = obj.GetComponentsInChildren<Transform>();
            foreach(Transform c in children)
            {
                if(c.gameObject.name.ToLower() != "background" && c.gameObject != obj)
                {
                    Destroy(c.gameObject);
                }
            }
        }

        foreach(KeyValuePair<int, ItemDto> kvp in Inventory)
        {
            GameObject parent = m_itemSlots[kvp.Key];

            // Delete old spawned images
            Image[] allImages = parent.GetComponentsInChildren<Image>();
            foreach(Image i in allImages)
            {
                if (i.gameObject.name.ToLower() != "background")
                    Destroy(i.gameObject);
            }

            // Create new images with right sprites
            GameObject inst = Instantiate(m_instantiatedItemPrefab, parent.transform);
            inst.GetComponent<Image>().sprite = kvp.Value.Image;
            
            TrayItem item = inst.AddComponent<TrayItem>();
            item.Item = kvp.Value;
            item.Slot = kvp.Key;
        }
    }

    private void OnPlayerInvMouseUp(OnPlayerInvItemMouseUp e)
    {
        if (m_selectedItemInfo?.Item == null)
        {
            if (!Inventory.ContainsKey(e.Slot))
                return;

            m_selectedItemInfo = new SelectedItemsInfo(e.Item, e.Slot, null);

            GameObject item = m_itemSlots[e.Slot];
            SetOutline(item.GetComponentInChildren<TrayItem>().gameObject, true);
        }
        else
        {
            if(e.Slot == m_selectedItemInfo?.Index && m_selectedItemInfo.Hero == null)
            {
                // Same item selected again, just deselect it
                GameObject item = m_itemSlots[e.Slot];
                SetOutline(item.GetComponentInChildren<TrayItem>().gameObject, false);
            }
            else if (m_selectedItemInfo?.Index >= 0)
            {
                if(m_selectedItemInfo?.Hero != null)
                {
                    // Moving from Hero Inventory to Player Inventory
                    GameObject item = m_itemSlots[m_selectedItemInfo.Index];
                    SetOutline(item.GetComponentInChildren<TrayItem>().gameObject, false);

                    if (Inventory.ContainsKey(e.Slot))
                    {
                        ItemDto heroItem = m_selectedItemInfo.Item;
                        ItemDto playerItem = Inventory[e.Slot];

                        // Move hero item to player inventory
                        Inventory.Remove(e.Slot);
                        Inventory.Add(e.Slot, heroItem);
                        // Then player inventory to hero
                        ClickerHeroBase hero = m_selectedItemInfo.Hero;
                        hero.RemoveItem(m_selectedItemInfo.Index);
                        hero.AddItem(e.Slot, playerItem);
                    }
                    else
                    {
                        m_selectedItemInfo.Hero.RemoveItem(m_selectedItemInfo.Index);
                        Inventory.Add(e.Slot, m_selectedItemInfo.Item);
                    }
                    
                    UpdateInventory();

                    m_selectedItemInfo = null;
                }
                else
                {
                    // Move in player inventory
                    ItemDto itemMoving = Inventory[m_selectedItemInfo.Index];
                    ItemDto itemTarget = null;
                    if (Inventory.ContainsKey(e.Slot))
                        itemTarget = Inventory[e.Slot];

                    Inventory.Remove(m_selectedItemInfo.Index);
                    if (itemTarget != null)
                        Inventory.Remove(e.Slot);

                    // Swap positions
                    Inventory.Add(e.Slot, itemMoving);
                    if (itemTarget != null)
                        Inventory.Add(m_selectedItemInfo.Index, itemTarget);

                    GameObject item = m_itemSlots[m_selectedItemInfo.Index];
                    SetOutline(item.GetComponentInChildren<TrayItem>().gameObject, false);

                    UpdateInventory();
                }
            }

            m_selectedItemInfo = null;
        }
    }

    private void OnHeroInvSlotMouseUp(OnMouseUpHeroInvSlot e)
    {
        if (m_selectedItemInfo != null)
        {
            if(m_selectedItemInfo?.Hero == e.Hero)
            {
                if (m_selectedItemInfo?.Index == e.TargetSlot)
                {
                    // Selected same item on hero, unselect
                    GameObject itemObj = e.Hero.ClickerUI.UIItemObjects[e.TargetSlot];
                    SetOutline(itemObj, false);

                    m_selectedItemInfo = null;
                }
                else
                {
                    // Selected new spot on hero, move there
                    e.Hero.RemoveItem(m_selectedItemInfo.Index);
                    e.Hero.AddItem(e.TargetSlot, m_selectedItemInfo.Item);

                    m_selectedItemInfo = null;
                }
            }
            else
            {
                if(e.Hero != null && m_selectedItemInfo?.Hero == null)
                {
                    // From Player Inventory to Hero Inventory
                    GameObject item = m_itemSlots[m_selectedItemInfo.Index];
                    SetOutline(item.GetComponentInChildren<TrayItem>().gameObject, false);
                    // If hero has item there
                    if (e.Hero.Items.ContainsKey(e.TargetSlot))
                    {
                        ItemDto heroItem = e.Hero.Items[e.TargetSlot];
                        ItemDto playerItem = Inventory[m_selectedItemInfo.Index];

                        Inventory.Remove(m_selectedItemInfo.Index);
                        Inventory.Add(m_selectedItemInfo.Index, heroItem);

                        e.Hero.RemoveItem(e.TargetSlot);
                        e.Hero.AddItem(e.TargetSlot, playerItem);
                    }
                    else
                    {
                        // No item in target slot
                        e.Hero.AddItem(e.TargetSlot, m_selectedItemInfo.Item);
                        Inventory.Remove(m_selectedItemInfo.Index);
                    }

                    UpdateInventory();

                    m_selectedItemInfo = null;
                }
                else if(e.Hero != null && m_selectedItemInfo?.Hero != null && e.Hero != m_selectedItemInfo?.Hero)
                {
                    // From one hero to another
                    if (m_selectedItemInfo.Hero.Items.ContainsKey(m_selectedItemInfo.Index) && e.Hero.Items.ContainsKey(e.TargetSlot))
                    {
                        // If both heroes contain an item, swap them over
                        ItemDto heroFromItem = m_selectedItemInfo.Hero.Items[m_selectedItemInfo.Index];
                        ItemDto heroToItem = e.Hero.Items[e.TargetSlot];

                        m_selectedItemInfo.Hero.RemoveItem(m_selectedItemInfo.Index);
                        m_selectedItemInfo.Hero.AddItem(m_selectedItemInfo.Index, heroToItem);

                        e.Hero.RemoveItem(e.TargetSlot);
                        e.Hero.AddItem(e.TargetSlot, heroFromItem);
                    }
                    else
                    {
                        // Remove from old hero and place at target hero
                        m_selectedItemInfo.Hero.RemoveItem(m_selectedItemInfo.Index);
                        e.Hero.AddItem(e.TargetSlot, m_selectedItemInfo.Item);
                    }

                    m_selectedItemInfo = null;
                }
            }
        }
        else if (e.Hero != null && e.Hero.Items.ContainsKey(e.TargetSlot))
        {
            // No selected item, select target
            if (e.Hero.ClickerUI.UIItemObjects.ContainsKey(e.TargetSlot))
            {
                GameObject itemObj = e.Hero.ClickerUI.UIItemObjects[e.TargetSlot];
                SetOutline(itemObj, true);

                m_selectedItemInfo = new SelectedItemsInfo(e.Hero.Items[e.TargetSlot], e.TargetSlot, e.Hero);
            }
        }
    }

    private void SetOutline(GameObject item, bool isOutlined)
    {
        if (isOutlined)
        {
            Material shaded = new Material(m_outlineShader);
            item.AddComponent<Image>().material = shaded;
        }
        else
        {
            Outline outline = item.gameObject.GetComponent<Outline>();
            Destroy(outline);
        }
    }

    public void RemoveItem(int index)
    {
        if (Inventory.ContainsKey(index))
        {
            Inventory.Remove(index);
            UpdateInventory();
        }
    }
}
