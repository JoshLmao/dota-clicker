﻿using System;
using System.Collections;
using UnityEngine;

public class RoshanEventManager : MonoBehaviour
{
    /// <summary>
    /// Amount of times the event has run
    /// </summary>
    public int EventCount { get; private set; }

    public bool IsRoshanAlive { get { return m_aliveRoshan != null; } }

    /// <summary>
    /// Start base health of Roshan
    /// </summary>
    public double BaseHealth = 500;

    [SerializeField]
    private GameObject m_roshanPrefab = null;

    [SerializeField]
    private GameObject m_aegisPrefab = null;

    [SerializeField]
    private GameObject m_cheesePrefab = null;

    [SerializeField]
    private Transform m_spawnTranform = null;

    [SerializeField]
    private Transform[] m_walkStagePoints = null;

    [SerializeField]
    private Transform[] m_walkHomePoints = null;

    private SceneController m_scene = null;
    private EventManager m_events = null;

    private Coroutine m_waitRoutine = null;
    private bool m_canDoEvent = false;
    private RoshanController m_aliveRoshan = null;

    private void Start()
    {
        m_scene = FindObjectOfType<SceneController>();

        m_events = SceneController.GetEventManager();
        m_events.AddListener<OnHeroBought>(OnBoughtHero);
        m_events.AddListener<OnRoshanEventEnded>(RoshanEventEnded);
    }

    private void OnBoughtHero(OnHeroBought e)
    {
        if (!m_canDoEvent && e.BoughtHero >= Hero.Tusk)
            m_canDoEvent = true;

        if(m_canDoEvent && m_waitRoutine == null)
            StartRoshanCountdown();
    }

    private void StartRoshanCountdown()
    {
        if (!m_canDoEvent)
            return;

        m_waitRoutine = StartCoroutine(Countdown(UnityEngine.Random.Range(Constants.SpawnTime.Min, Constants.SpawnTime.Max)));
    }

    private IEnumerator Countdown(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        StartEvent();
    }

    public void StartEvent()
    {
        GameObject instRoshan = Instantiate(m_roshanPrefab, m_spawnTranform, false);
        instRoshan.transform.SetPositionAndRotation(m_spawnTranform.transform.position, m_spawnTranform.transform.rotation);

        m_aliveRoshan = instRoshan.GetComponent<RoshanController>();
        m_aliveRoshan.Init(this, m_walkStagePoints, m_walkHomePoints);
    }

    private void RoshanEventEnded(OnRoshanEventEnded e)
    {
        EventCount++;

        if (e.IsKilled)
        {
            //Instantiate cheese & aegis
            SpawnWithVelocity(m_aegisPrefab);
            if (EventCount > 20)
            {
                // Spawn Cheese after certain amount of events
                SpawnWithVelocity(m_cheesePrefab);
            }
        }
    }

    private GameObject SpawnWithVelocity(GameObject prefab)
    {
        GameObject obj = Instantiate(prefab);

        float rotationOffset = UnityEngine.Random.Range(10f, 25f);
        float velocity = UnityEngine.Random.Range(6f, 20f);

        obj.transform.position = m_aliveRoshan.transform.position + new Vector3(0f, 7f, 0f);
        obj.transform.rotation = new Quaternion(obj.transform.rotation.x, obj.transform.rotation.y - rotationOffset, obj.transform.rotation.z, obj.transform.rotation.w);
        obj.GetComponent<Rigidbody>().velocity += new Vector3(0f, 0f, velocity);
        return obj;
    }
}
