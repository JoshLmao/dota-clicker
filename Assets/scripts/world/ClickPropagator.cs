﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// Detects a click and triggers the event
/// </summary>
public class ClickPropagator : MonoBehaviour, IPointerClickHandler
{
    public event Action<GameObject> MouseDown;
    public event Action<GameObject> MouseUp;
    public event Action<GameObject> MouseEnter;
    public event Action<GameObject> MouseExit;

    public event Action<GameObject> RightMouseUp;

    public void OnMouseDown()
    {
        MouseDown?.Invoke(this.gameObject);
    }
    public void OnMouseUp()
    {
        MouseUp?.Invoke(this.gameObject);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(eventData.button == PointerEventData.InputButton.Right)
        {
            RightMouseUp?.Invoke(this.gameObject);
        }
    }

    private void OnMouseEnter()
    {
        MouseEnter?.Invoke(this.gameObject);
    }

    private void OnMouseExit()
    {
        MouseExit?.Invoke(this.gameObject);
    }
}
