﻿using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[System.Serializable]
public class IndexAbilityClipDict : SerializableDictionaryBase<int, AudioClip>
{

}
