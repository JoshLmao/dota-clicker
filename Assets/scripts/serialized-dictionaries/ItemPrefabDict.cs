﻿using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine;

[System.Serializable]
public class ItemPrefabDict : SerializableDictionaryBase<Item, GameObject>
{

}
