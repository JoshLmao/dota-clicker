﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utilities
{
    public static T GetAddComponent<T>(GameObject obj) where T : Behaviour
    {
        T component = obj.GetComponent<T>();
        if (component == null)
            component = obj.AddComponent<T>();

        return component;
    }

    /// <summary>
    /// Ignore the parameter game object when searching for child components
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    public static T GetActualChildComponent<T>(GameObject obj) where T : Behaviour
    {
        T[] components = obj.GetComponentsInChildren<T>();
        for (int i = 0; i < components.Length; i++)
        {
            T current = components[i];
            if (current.name != obj.name)
            {
                return current;
            }
        }
        return null;
    }

    public static double GetPercentOfVal(double value, double percent)
    {
        return (value / 100.0) * percent;
    }

    /// <summary>
    /// Plays a random clip on the source from the AudioClips
    /// </summary>
    /// <param name="audioSource"></param>
    /// <param name="clips"></param>
    public static void PlayRandomClip(AudioSource audioSource, AudioClip[] clips)
    {
        int pick = UnityEngine.Random.Range(0, clips.Length);
        if (!audioSource.isPlaying)
        {
            audioSource.PlayOneShot(clips[pick]);
        }
    }
}
