﻿using RotaryHeart.Lib.SerializableDictionary;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraPositionController : MonoBehaviour
{
    public enum Position
    {
        Clickers,
        HomeCamp,
        SideShop,
        SecretShop,
    }
    
    [Serializable]
    public class TransformPositions : SerializableDictionaryBase<Position, Transform> { }
    [Serializable]
    public class ButtonPositions : SerializableDictionaryBase<Position, Button> { }

    public TransformPositions CameraPositions = null;
    public ButtonPositions Buttons = null;

    [SerializeField]
    private Camera m_camera;

    public void Start()
    {
        foreach(KeyValuePair<Position, Button> kvp in Buttons)
        {
            kvp.Value.onClick.AddListener(() => SetLocation(kvp.Key));
        }
    }

    public void SetLocation(Position position)
    {
        Transform targetTransform = CameraPositions[position];
        m_camera.transform.SetPositionAndRotation(targetTransform.position, targetTransform.rotation);
    }
}
