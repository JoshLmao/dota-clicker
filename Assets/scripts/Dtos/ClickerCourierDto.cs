﻿using UnityEngine;

public class ClickerCourierDto : IAttackSpeedSupplier, IDamageSupplier
{
    public string Name { get; set; }
    public string Description { get; set; }
    public Courier Courier { get; set; }

    public Sprite Image { get; set; }
    public int Cost { get; set; }

    public double AtkSpeedBonus { get; set; }
    public double DamageBonus { get; set; }
    public float AttacksPerMinute { get; set; }
}
