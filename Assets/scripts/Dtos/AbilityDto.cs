﻿using UnityEngine;

public class AbilityDto
{
    /// <summary>
    /// Display name of the Ability
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Hero the ability is for
    /// </summary>
    public Hero Hero { get; set; }
    /// <summary>
    /// Index of the ability with all the other ones
    /// </summary>
    public int Index { get; set; }
    /// <summary>
    /// Text description for the ability
    /// </summary>
    public string Description { get; set; }
    /// <summary>
    /// Display UI image for the ability
    /// </summary>
    public Sprite Image { get; set; }
    /// <summary>
    /// Amount to buy this ability
    /// </summary>
    public int Cost { get; set; }

    /// <summary>
    /// Amount of seconds to cooldown
    /// </summary>
    public int BaseCooldown { get; set; }
}
