﻿public class FloatRange
{
    public float Min { get; set; }
    public float Max { get; set; }

    public FloatRange(float min, float max)
    {
        Min = min;
        Max = max;
    }
}

public class IntRange
{
    public int Min { get; set; }
    public int Max { get; set; }

    public IntRange(int min, int max)
    {
        Min = min;
        Max = max;
    }
}
