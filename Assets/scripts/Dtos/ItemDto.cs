﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ItemDto : IAttackSpeedSupplier, IDamageSupplier
{
    public string Name { get; set; }
    public Sprite Image { get; set; }
    public string Description { get; set; }
    public int Cost { get; set; }

    public GameObject ItemPrefab { get; set; }

    public double AtkSpeedBonus { get; set; } = 0.0;
    public double DamageBonus { get; set; } = 0.0;

    public ItemDto() { }

    public ItemDto(ItemDto item)
    {
        Name = item.Name;
        Image = item.Image;
        Description = item.Description;
        Cost = item.Cost;
        ItemPrefab = item.ItemPrefab;

        AtkSpeedBonus = item.AtkSpeedBonus;
        DamageBonus = item.DamageBonus;
    }
}
