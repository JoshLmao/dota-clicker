﻿using UnityEngine;

public class GlobalSoundManager 
{
    private static AudioClip m_buySound = null;
    private static AudioClip m_invalidSound = null;

    public GlobalSoundManager()
    {
        m_buySound = Resources.Load<AudioClip>("Sounds/UI/buy");
        m_invalidSound = Resources.Load<AudioClip>("Sounds/UI/magic_immune");
    }

    public static AudioClip GetInvalidSound()
    {
        return m_invalidSound;
    }

    public static AudioClip GetShopBuySound()
    {
        return m_buySound;
    }
}
