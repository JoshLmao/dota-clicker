﻿public class OnStartDragItem : GameEventBase
{
    public ItemDto Item { get; set; }
    public OnStartDragItem(ItemDto item)
    {
        Item = item;
    }
}
