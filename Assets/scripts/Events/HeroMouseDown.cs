﻿public class HeroMouseDown : GameEventBase
{
    public Hero Hero { get; set; }
    public HeroMouseDown(Hero hero)
    {
        Hero = hero;
    }
}
