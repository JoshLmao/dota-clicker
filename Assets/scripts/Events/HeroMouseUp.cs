﻿public class HeroMouseUp : GameEventBase
{
    public Hero Hero { get; set; }
    public HeroMouseUp(Hero h)
    {
        Hero = h;
    }
}
