﻿public class OnMouseExitHeroInvSlot : GameEventBase
{
    public int Slot { get; set; }
    public OnMouseExitHeroInvSlot(int slot)
    {
        Slot = slot;
    }
}
