﻿public class HeroMouseEnter : GameEventBase
{
    public Hero Hero { get; set; }
    public HeroMouseEnter(Hero h)
    {
        Hero = h;
    }
}
