﻿public class HeroMouseExit : GameEventBase
{
    public Hero Hero { get; set; }
    public HeroMouseExit(Hero h)
    {
        Hero = h;
    }
}
