﻿public class OnPlayerInvItemMouseUp : GameEventBase
{
    public int Slot { get; set; }
    public ItemDto Item { get; set; }
    public OnPlayerInvItemMouseUp(int slot, ItemDto item)
    {
        Slot = slot;
        Item = item;
    }
}