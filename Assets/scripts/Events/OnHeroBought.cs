﻿public class OnHeroBought : GameEventBase
{
    public Hero BoughtHero { get; set; } = Hero.Unknown;
    public OnHeroBought(Hero hero)
    {
        BoughtHero = hero;
    }
}
