﻿public class OnRoshanEventEnded : GameEventBase
{
    public bool IsKilled { get; set; }
    public OnRoshanEventEnded(bool isKilled)
    {
        IsKilled = isKilled;
    }
}