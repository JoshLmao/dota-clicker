﻿using UnityEngine;

public class OnSellItem : GameEventBase
{
    public Hero Hero { get; set; }
    public ItemDto Item { get; set; }
    public int SlotIndex { get; set; }
    public OnSellItem(ItemDto item, int slotIndex, Hero h)
    {
        Item = item;
        SlotIndex = slotIndex;
        Hero = h;
    }
}
