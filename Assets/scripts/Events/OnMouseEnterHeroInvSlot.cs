﻿public class OnMouseEnterHeroInvSlot : GameEventBase
{
    public int Slot { get; set; }
    public ClickerHeroBase Clicker { get; set; }
    public OnMouseEnterHeroInvSlot(int slot)
    {
        Slot = slot;
    }
}
