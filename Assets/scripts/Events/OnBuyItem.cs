﻿public class OnBuyItem : GameEventBase
{
    public ItemDto Item { get; set; }

    public OnBuyItem(ItemDto item)
    {
        Item = item;
    }
}
