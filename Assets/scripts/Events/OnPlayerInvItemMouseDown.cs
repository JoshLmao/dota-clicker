﻿public class OnPlayerInvItemMouseDown : GameEventBase
{
    public ItemDto Item { get; set; }
    public OnPlayerInvItemMouseDown(ItemDto item)
    {
        Item = item;
    }
}