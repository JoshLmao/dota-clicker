﻿public class OnMouseUpHeroInvSlot : GameEventBase
{
    public int TargetSlot { get; set; }
    public ClickerHeroBase Hero { get; set; }

    public OnMouseUpHeroInvSlot(ClickerHeroBase hero, int slot)
    {
        Hero = hero;
        TargetSlot = slot;
    }
}
