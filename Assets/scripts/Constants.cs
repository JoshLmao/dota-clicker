﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public class ItemStat
    {
        public int Cost { get; set; }
        public double AttackSpeed { get; set; }
        public double Damage { get; set; }
        public ItemStat(double atkSpd, double dmg, int cost)
        {
            AttackSpeed = atkSpd;
            Damage = dmg;
            Cost = cost;
        }

        public override string ToString()
        {
            return $"+{AttackSpeed} Attack Speed, +{Damage} Damage";
        }
    }

    public class AbilityStat
    {
        public int Cost;
        public int BaseCooldown;
        public AbilityStat(int cost, int cooldown)
        {
            Cost = cost;
            BaseCooldown = cooldown;
        }
    }

    public static int LAST_HIT_VALUE { get { return 1; } }

    #region XP
    /// <summary>
    /// Amount of XP earned from "killing a creep" (normal last hit)
    /// </summary>
    public static FloatRange LastHitXP { get { return new FloatRange(0.1f, 1.0f); } }
    /// <summary>
    /// The base amount needed to level up a hero to level 1
    /// </summary>
    public static float BASE_XP_AMOUNT { get { return 10f; } }
    /// <summary>
    /// Maximum level a hero can be
    /// </summary>
    public static int MAX_LEVEL { get { return 25; } }
    /// <summary>
    /// Base health range for creeps
    /// </summary>
    public static FloatRange CreepHealth { get { return new FloatRange(100f, 150f); } }
    /// <summary>
    /// Base bounty for killing creeps
    /// </summary>
    public static IntRange BaseCreepBounty { get { return new IntRange(2, 6); } }
    #endregion

    #region Damage
    /// <summary>
    /// The percentage amount of the base damage of the hero it gets per it's level up
    /// Eg. 0.10 (10%) of it's base damage every level
    /// </summary>
    public static double PercentBaseDamagePerLevel { get { return 7.0; } }
    #endregion

    #region Items
    // Attack Speed, Damage, Cost
    public static ItemStat IronBranch { get { return new ItemStat(0.1, 1.0, 50); } }
    public static ItemStat QuellingBlade { get { return new ItemStat(0.0, 3.0, 900); } }
    public static ItemStat PowerTreads { get { return new ItemStat(2.5, 5.0, 1500); } }
    public static ItemStat Bottle { get { return new ItemStat(0.0, 0.0, 2500); } }
    public static ItemStat BlinkDagger { get { return new ItemStat(0.0, 0.0, 2750); } }
    public static ItemStat Hyperstone { get { return new ItemStat(0.0, 0.0, 3250); } }
    public static ItemStat Bloodstone { get { return new ItemStat(0.0, 0.0, 3750); } }
    public static ItemStat Reaver { get { return new ItemStat(0.0, 0.0, 4500); } }
    public static ItemStat DivineRapier { get { return new ItemStat(0.0, 0.0, 5000); } }
    public static ItemStat Recipe{ get { return new ItemStat(0.0, 0.0, 6000); } }
    public static ItemStat Mjollnir { get { return new ItemStat(0.50, 5.0, 5000); } }
    public static ItemStat BlackKingBar { get { return new ItemStat(0.0, 0.0, 3950); } }
    public static ItemStat ShivasGuard { get { return new ItemStat(0.0, 0.0, 4200); } }
    #endregion

    #region Couriers
    // Attack Speed, Damage, Cost,
    public static ItemStat DonkeyRadiant { get { return new ItemStat(0.0, 0.0, 100); } }
    public static ItemStat DonkeyDire { get { return new ItemStat(0.0, 0.0, 100); } }
    public static ItemStat TrapJaw { get { return new ItemStat(0.0, 0.0, 100); } }
    public static ItemStat DoomDemihero { get { return new ItemStat(0.0, 0.0, 100); } }
    public static ItemStat BabyWyvern { get { return new ItemStat(0.0, 0.0, 100); } }
    public static ItemStat BabyRoshan { get { return new ItemStat(0.0, 0.0, 100); } }
    #endregion

    #region Roshan
    public static IntRange SpawnTime { get { return new IntRange(600, 1000); } }
    #endregion

    #region Hero Abilities
    public static AbilityStat CrystalNova { get { return new AbilityStat(25, 10); } }
    public static AbilityStat Frostbite { get { return new AbilityStat(45, 10); } }
    #endregion

    #region Misc
    /// <summary>
    /// Gets a random time in seconds in the range for playing the rare idle anim
    /// </summary>
    public static int RareIdleAnimTime { get { return Random.Range(20, 60); } }
    #endregion

    //Abilities
    public static double CrystalNovaMultiplier = 2;
    public static double FrostbiteMultiplier = 2;
    public static double TelekinesisMultiplier = 2;
    public static double SpellStealMultiplier = 2;
    public static double FireblastMultiplier = 2;
    public static double BloodlustMultiplier = 2;
    public static double SnowballMultiplier = 2;
    public static double WalrusPunchMultiplier = 2;
    public static double SunrayMultiplier = 2;
    public static double SupernovaMultiplier = 2;
    public static double WarCryMultiplier = 2;
    public static double GodsStrengthMultiplier = 2;
    public static double BlinkMultiplier = 2;
    public static double ManaVoidMultiplier = 2;
    public static double GreevilsGreedMultiplier = 2;
    public static double ChemicalRageMultiplier = 2;

    //Difference between Ability levels (Usage count)
    public static int Ability0To1Difference = 25;
    public static int Ability1To2Difference = 45;
    public static int Ability2To3Difference = 55;
    public static int Ability3To4Difference = 75;

    //Hero Abilities Cost
    public static int FrostbiteCost = 45;
    public static int TelekinesisCost = 150;
    public static int SpellStealCost = 350;
    public static int FireblastCost = 500;
    public static int BloodlustCost = 750;
    public static int SnowballCost = 1500;
    public static int WalrusPunchCost = 2500;
    public static int SunrayCost = 6000;
    public static int SupernovaCost = 7000;
    public static int WarCryCost = 10000;
    public static int GodsStrengthCost = 12500;
    public static int BlinkCost = 15000;
    public static int ManaVoidCost = 17500;
    public static int GreevilsGreedCost = 25000;
    public static int ChemicalRageCost = 35000;
}

