﻿public enum Hero
{
    Unknown = -1,
    CrystalMaiden = 0,
    Rubick = 1,
    OgreMagi = 2,
    Tusk,
    Phoenix,
    Sven,
    AntiMage,
    Alchemist,
}
