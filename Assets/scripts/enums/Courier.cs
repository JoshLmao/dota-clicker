﻿public enum Courier
{
    Unknown = -1,
    DonkeyRadiant,
    DonkeyDire,
    Trapjaw,
    DoomDemihero,
    BabyWyvern,
    BabyRoshan,
}
