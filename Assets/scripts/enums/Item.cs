﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Item
{
    Unknown,
    IronBranch,
    QuellingBlade,
    PowerTreads,
    Bottle,
    BlinkDagger,
    HyperStone,
    Bloodstone,
    Reaver,
    Divine,
    Recipe,
    BlackKingBar,
    ShivasGuard,
    Mjollnir,
}