using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class CourierService : MonoBehaviour
{
    [System.Serializable]
    public class CourierPrefabMap : RotaryHeart.Lib.SerializableDictionary.SerializableDictionaryBase<Courier, GameObject> { }

    public List<ClickerCourierDto> BoughtCouriers { get; private set; }

    public CourierPrefabMap CourierMap = null;

    public event System.Action<ClickerCourierDto> OnBoughtCourier;

    private void Start()
    {
        BoughtCouriers = new List<ClickerCourierDto>();
    }

    public void BuyCourier(ClickerCourierDto courier)
    {
        BoughtCouriers.Add(courier);

        OnBoughtCourier?.Invoke(courier);
    }

    public GameObject GetCourierPrefab(Courier courier)
    {
        if (CourierMap.ContainsKey(courier))
            return CourierMap[courier];
        else
            return null;
    }

    public void SetToHero(ClickerCourierDto courier, Hero hero)
    {
        if (BoughtCouriers.Contains(courier))
        {
            ClickerHeroBase heroBase = FindObjectOfType<SceneController>().Heroes.FirstOrDefault(x => x.Hero == hero);
            heroBase.SetClickerCourier(courier);

            BoughtCouriers.Remove(courier);
        }
    }

    public GameObject SpawnCourier(ClickerCourierDto courier, Transform spawnPosParent)
    {
        GameObject crowPrefab = GetCourierPrefab(courier.Courier);
        if (crowPrefab != null)
        {
            GameObject crow = Instantiate(crowPrefab, spawnPosParent);
            return crow;
        }
        return null;
    }
}
