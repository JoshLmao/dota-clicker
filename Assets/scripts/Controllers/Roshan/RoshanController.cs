﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RoshanController : MonoBehaviour
{
    private enum WalkState
    {
        Walking,
        OnStage,
        AtHome,
    }

    public double TotalHealth { get; private set; }
    public double CurrentHealth { get; private set; }

    /// <summary>
    /// Bool to see if Roshan has been killed
    /// </summary>
    public bool isDead = false;

    [SerializeField]
    private GameObject m_healthUIPrefab = null;

    [SerializeField]
    private Transform m_healthUIParent = null;

    [SerializeField]
    private float m_walkSpeed = 4f;

    private RoshanEventManager m_eventManager = null;
    private SceneController m_scene = null;
    private EventManager m_events = null;

    private AudioSource m_audioSource = null;
    private Animator m_animator = null;
    private Image m_activeHealth = null;

    private WalkState m_walkState = WalkState.AtHome;
    private Coroutine m_walkRoutine = null;

    private DateTime m_eventStartTime = DateTime.MinValue;
    private DateTime m_predictedEndTime = DateTime.MinValue;

    private Transform[] m_walkStagePoints = null;
    private Transform[] m_walkHomePoints = null;

    private GameObject m_healthUI = null;

    private float m_playerGoldOnStart = -1;
    private bool m_canDamage = false;

    private const int BASE_DURATION = 120;
    private const string REACHED_POINT_TRIGGER = "hasReachedPosition";
    private const string IS_DEAD_BOOL = "isDead";
    private const string FIREBREATH_ANIM_TRIGGER = "doFirebreath";
    private const string WALKING_ANIM_BOOL = "isWalking";

    #region MonoBehavious
    private void Start()
    {
        m_audioSource = this.GetComponent<AudioSource>();
        m_animator = this.GetComponent<Animator>();
        m_events = SceneController.GetEventManager();
        m_scene = FindObjectOfType<SceneController>();

        m_healthUI = Instantiate(m_healthUIPrefab, m_healthUIParent, false);
        RectTransform rectT = m_healthUI.GetComponent<RectTransform>();
        rectT.anchorMin = new Vector2(0, 0);
        rectT.anchorMax = new Vector2(1, 1);
        rectT.sizeDelta = Vector2.zero;
        m_healthUI.SetActive(false);

        m_activeHealth = m_healthUI.GetComponentInChildren<Image>();

        // OnRoshanStarted Event

        //Health multiplied by eventCount divided by 1.6. Don't want to double health after every event
        if (m_eventManager.EventCount > 0)
            TotalHealth = m_eventManager.BaseHealth + (m_eventManager.EventCount / 1.6f);

        m_playerGoldOnStart = (float)m_scene.TotalGold;

        m_walkRoutine = StartCoroutine(WalkThroughTargets(m_walkStagePoints, WalkState.OnStage, OnReachedStage));
    }

	private void Update()
    {
        float currentPlayerGold = (float)m_scene.TotalGold;

        if(currentPlayerGold > m_playerGoldOnStart && !isDead && m_canDamage)
        {
            float difference = currentPlayerGold - m_playerGoldOnStart;
            double actualHP = CurrentHealth - difference;
            CurrentHealth = (actualHP - 0) / (CurrentHealth - 0);

            m_activeHealth.fillAmount = (float)(CurrentHealth / TotalHealth);

            if (m_activeHealth.fillAmount <= 0)
            {
                OnKilled();
            }
        }
    }
    #endregion

    public void Init(RoshanEventManager manager, Transform[] walkStagePoints, Transform[] walkHomePoints)
    {
        m_eventManager = manager;
        m_walkStagePoints = walkStagePoints;
        m_walkHomePoints = walkHomePoints;
    }

    /// <summary>
    /// Roshan reached main stage and waiting event time
    /// </summary>
    private void OnReachedStage()
    {
        m_animator.SetTrigger(REACHED_POINT_TRIGGER);
        m_walkState = WalkState.OnStage;

        //Start countdown till event is over in seconds, scales with amount of events
        float eventDurationSeconds = 10f;//BASE_DURATION;
        if(m_eventManager.EventCount > 0)
            eventDurationSeconds = (eventDurationSeconds / m_eventManager.EventCount) * 2;

        StartCoroutine(EventEndTime(eventDurationSeconds));
        m_eventStartTime = DateTime.Now;
        m_predictedEndTime = DateTime.Now.AddSeconds(eventDurationSeconds);

        m_canDamage = true;
        m_healthUI.SetActive(true);
    }

    /// <summary>
    /// Player killed Roshan in time
    /// </summary>
    public void OnKilled()
    {
        m_healthUI.gameObject.SetActive(false);
        m_animator.SetBool(IS_DEAD_BOOL, true);
        m_audioSource.PlayOneShot(Resources.Load<AudioClip>("Sounds/Gameplay/Roshan/Roshan_Death"));

        m_events.TriggerEvent(new OnRoshanEventEnded(true));

        // Wait for Roshan to do death anim then remove
        StartCoroutine(WaitForRoshanDeathAnim());
    }

    /// <summary>
    /// Player failed to kill Roshan in time
    /// </summary>
    private void OnTimeOut()
    {
        m_animator.SetTrigger(FIREBREATH_ANIM_TRIGGER);
        // Disable player damage, remove health UI
        m_canDamage = false;
        m_healthUI.SetActive(false);

        StartCoroutine(WaitEndAnimWalkHome());
    }

    /// <summary>
    /// Roshan walked home
    /// </summary>
    private void OnReachedHome()
    {
        m_animator.SetTrigger(REACHED_POINT_TRIGGER);
        m_walkState = WalkState.AtHome;

        DestroyRoshan();

        //OnRoshanNotKilled event
        m_events.TriggerEvent(new OnRoshanEventEnded(false));
    }

    IEnumerator WaitForRoshanDeathAnim()
    {
        // Wait for death sound & anim and destroy
        yield return new WaitForSeconds(5f);
        DestroyRoshan();
    }

    IEnumerator EventEndTime(float timeSeconds)
    {
        Debug.Log("Roshan Event: Waiting '" + timeSeconds + "' for event to end");
        yield return new WaitForSeconds(timeSeconds);
        OnTimeOut();
    }

    IEnumerator WaitEndAnimWalkHome()
    {
        //Firebreath animation duration //6.4 seconds
        yield return new WaitForSeconds(6.4f);

        m_walkRoutine = StartCoroutine(WalkThroughTargets(m_walkHomePoints, WalkState.AtHome, OnReachedHome));
    }

    private void DestroyRoshan()
    {
        foreach (Transform child in transform)
        {
            DestroyImmediate(child.gameObject);
        }
        DestroyImmediate(this.gameObject);
    }

    IEnumerator WalkThroughTargets(Transform[] targets, WalkState arriveState, Action onFinishedWalking)
    {
        m_walkState = WalkState.Walking;

        m_animator.SetBool(WALKING_ANIM_BOOL, true);

        int startIndex = 0;
        while(startIndex < targets.Length)
        {
            Transform target = targets[startIndex];
            while (transform.position != target.position)
            {
                transform.forward = Vector3.RotateTowards(transform.forward, target.position - transform.position, m_walkSpeed * Time.deltaTime, 0f);
                transform.position = Vector3.MoveTowards(transform.position, target.position, m_walkSpeed * Time.deltaTime);

                yield return new WaitForEndOfFrame();
            }

            startIndex++;
            yield return new WaitForEndOfFrame();
        }

        m_animator.SetBool(WALKING_ANIM_BOOL, false);
        m_walkState = arriveState;

        onFinishedWalking?.Invoke();
    }
}
