﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Generic class aimed at being used by each clicker. Controls times and multipliers
/// </summary>
public class RadiantClickerController : MonoBehaviour
{
    /**  Clicker Details & Start Values**/
    public string HeroName;
    public int StartClickAmount = 1;
    public float UpgradeCost = 10;
    public float UpgradeMultiplier = 1.15f;
    /****************************/
    /// <summary>
    /// Current amount the clicker will give if clicked
    /// </summary>
    public int ClickAmount
    {
        get
        {
            if (ClickerMultiplier == 0) return StartClickAmount * 1;
            else return StartClickAmount * ClickerMultiplier;
        }
        set
        {
            if(value > 0)
                StartClickAmount = value;
        }
    }
    /// <summary>
    /// Amount of times the clicker has been bought
    /// </summary>
    public int ClickerMultiplier = 0;
    /// <summary>
    /// Public variable for TimeSpan
    /// </summary>
    public int SecondsToCompleteClick;
    /// <summary>
    /// Start time it takes to complete one click. 
    /// </summary>
    public TimeSpan TimeBetweenClicks; //5 seconds
    /// <summary>
    /// Current time the timer is at. Used in displays
    /// </summary>
    public TimeSpan CurrentClickerTime;
    /// <summary>
    /// Bool to determine if Click button has been clicked
    /// </summary>
    public bool IsClicked = false;
    /// <summary>
    /// Cost to buy Manager upgrade
    /// </summary>
    public int ManagerCost;
    /// <summary>
    /// Is the hero automated?
    /// </summary>
    public bool HasManager = false;
    /// <summary>
    /// Can the hero be clicked
    /// </summary>
    public bool CanBeClicked = false;
    /// <summary>
    /// Amount of times the Ability has to be pressed to level up at Lvl 1
    /// </summary>
    public int Ability1LvlUpCount;
    /// <summary>
    /// Amount of times the Ability has to be pressed to level up at Lvl 1
    /// </summary>
    public int Ability2LvlUpCount;
    /// <summary>
    /// For stats and saving
    /// </summary>
    public float Ability1UseCount;
    /// <summary>
    /// For stats and saving
    /// </summary>
    public float Ability2UseCount;

    public delegate void OnClickButton(string clickerName);
    public event OnClickButton OnClickedButton;
    public delegate void OnClickFinished(string clickerName);
    public event OnClickFinished OnClickedFinished;

    private SceneController m_sceneController;
    private DateTime m_lastClickedTime;

    private Slider m_progressBar;
    private Text m_heroNameText;
    private Text m_timeRemainingText;
    private Text m_amountBoughtText;
    private Text m_clickButtonGoldText;
    private Text m_upgradeCostText;

    //Level Up System
    /// <summary>
    /// Ability 1 Level
    /// </summary>
    public int Ability1Level = 0;
    public bool Ability1InUse = false;
    /// <summary>
    /// Ability 2 Level
    /// </summary>
    public int Ability2Level = 0;
    public bool Ability2InUse = false;
    Slider m_abil1Slider;
    Slider m_abil2Slider;
    List<GameObject> m_abil1Icons = new List<GameObject>();
    List<GameObject> m_abil2Icons = new List<GameObject>();
    /// <summary>
    /// Amount needed to level up from previous to next level - Ability 1
    /// </summary>
    public int m_abil1UseCount = 0;
    /// <summary>
    /// Amount needed to level up from previous to next level - Ability 2
    /// </summary>
    public int m_abil2UseCount = 0;
    Sprite m_notLevelled;
    Sprite m_levelled;

    //Cant Use Ability sound
    AudioClip MagicImmuneSound;

    Image m_activeModifier = null;
    Transform m_itemModifierHolder;
    GameObject m_activeItemModifierPrefab;

    //Track Start Times of Coroutines
    public DateTime m_currentModifierRoutineStarted;
    public int m_currentModifierTotalTime = -1;
    public string m_currentModifier = string.Empty;
    public TimeSpan m_currentClickTimePassed;
    public DateTime m_ability1ClickTime = DateTime.MinValue;
    public DateTime m_ability2ClickTime = DateTime.MinValue;

    bool m_modifierCountdownActive = false;
    Text m_modifierCountdownText = null;

    //Calculates end result of amount * multiplier for UI
    double m_clickAmount;
   
    void Awake()
    {
        m_sceneController = FindObjectOfType<SceneController>();

        m_heroNameText = transform.Find("Buttons/StandBack/StandUI/ClickerNameText").GetComponent<Text>();
        m_timeRemainingText = transform.Find("Buttons/StandBack/StandUI/ProgressSlider/TimeRemaining").GetComponent<Text>();
        m_amountBoughtText = transform.Find("Buttons/StandBack/StandUI/AmountCanvas/AmountText").GetComponent<Text>();
        m_clickButtonGoldText = transform.Find("Buttons/ClickButtonBack/ClickButton/ClickUI/ClickWorthText").GetComponent<Text>();
        m_upgradeCostText = transform.Find("Buttons/UpgradeCostBack/UpgradeCostCanvas/Cost/UpCostText").GetComponent<Text>(); ;
        m_progressBar = transform.Find("Buttons/StandBack/StandUI/ProgressSlider").GetComponent<Slider>();
        MagicImmuneSound = Resources.Load<AudioClip>("Sounds/UI/magic_immune");

        m_activeModifier = transform.Find("Buttons/StandBack/StandUI/ActiveModifierUI/ActiveModifier").GetComponent<Image>();
        m_activeModifier.color = new Color(255, 255, 255, 0);
        //m_itemModifierHolder = transform.Find("ItemModifierStand/ItemHolderTransform").gameObject.transform;

        //m_modifierCountdownText = transform.Find("ItemModifierStand/ModifierTimeCanvas/ModifierTimeRemaining").GetComponent<Text>();
        //m_modifierCountdownText.gameObject.SetActive(false);
        m_abil1Slider = transform.Find("Buttons/StandBack/UpgradesCanvas/Abil1Progress").GetComponent<Slider>();
        m_abil2Slider = transform.Find("Buttons/StandBack/UpgradesCanvas/Abil2Progress").GetComponent<Slider>();
    }

    void Start ()
    {
        AbilityLevelUpStart();

        TimeBetweenClicks = new TimeSpan(0, 0, 0, SecondsToCompleteClick);
    }
	
    void AbilityLevelUpStart()
    {
        Transform abil1Transform = transform.Find("Buttons/StandBack/UpgradesCanvas/Abil1Levels");
        Transform abil2Transform = transform.Find("Buttons/StandBack/UpgradesCanvas/Abil2Levels");
        foreach (Transform trans in abil1Transform)
        {
            m_abil1Icons.Add(trans.gameObject);
        }
        foreach (Transform trans in abil2Transform)
        {
            m_abil2Icons.Add(trans.gameObject);
        }

        m_notLevelled = Resources.Load<Sprite>("Images/UI/other/NotLevelled");
        m_levelled = Resources.Load<Sprite>("Images/UI/other/Levelled");

        m_abil1Slider.maxValue = Ability1LvlUpCount;
        m_abil1Slider.value = 0;

        m_abil2Slider.maxValue = Ability2LvlUpCount;
        m_abil2Slider.value = 0;
    }

    void Update ()
    {
        UpdateUIText();

        if(IsClicked)
        {
            CurrentClickerTime = DateTime.Now - m_lastClickedTime;
            m_currentClickTimePassed = CurrentClickerTime; //For saving, how much time has passed on current click
            if (CurrentClickerTime >= TimeBetweenClicks)
            {
                IsClicked = false;
                CompletedClick();
            }
            UpdateCountdownTimer();
        }
        else
        {
            //For loading a save file in
            m_currentClickTimePassed = TimeSpan.MinValue;
        }

        if(HasManager)
        {
            if(!IsClicked)
            {
                OnClickButtonPressed();
            }
        }

        //if (m_modifierCountdownActive)
        //{
        //    var timeRemaining = m_currentModifierRoutineStarted - DateTime.Now;
        //    m_modifierCountdownText.text = timeRemaining.Seconds.ToString();
        //    Console.WriteLine("timeRemaining = " + timeRemaining);
        //}
    }

    public void UpdateCountdownTimer()
    {
        var percent = Divide(CurrentClickerTime, TimeBetweenClicks) * 100f;
        m_progressBar.value = percent;

        if(percent <= 100f)
        {
            m_progressBar.value = percent;
        }
        else
        {
            m_progressBar.value = 0;
        }
    }

    public static float Divide(TimeSpan dividend, TimeSpan divisor)
    {
        return (float)dividend.Ticks / (float)divisor.Ticks;
    }

    public void OnBuyClickerButtonPressed()
    {
        UpgradeCost = Mathf.Round(UpgradeCost * UpgradeMultiplier);
    }

    void UpdateUIText()
    {
        m_heroNameText.text = HeroName;

        CalculateClickResultUI();

        m_clickButtonGoldText.text = m_clickAmount + " gold";
        m_amountBoughtText.text = ClickerMultiplier.ToString();
        m_upgradeCostText.text = UpgradeCost.ToString() + " gold";

        if (CurrentClickerTime <= TimeBetweenClicks)
            m_timeRemainingText.text = CurrentClickerTime.ToString();
        else
            m_timeRemainingText.text = new TimeSpan(0, 0, 0, 0).ToString();
    }

    void CalculateClickResultUI()
    {
        double abilityModify = 0;
        double itemModify = 0;

        if (abilityModify > 0 && itemModify <= 0)
            m_clickAmount = ClickAmount * abilityModify;
        else if (abilityModify <= 0 && itemModify > 0)
            m_clickAmount = ClickAmount * itemModify;
        else if (abilityModify > 0 && itemModify > 0)
            m_clickAmount = ClickAmount * (abilityModify + itemModify);
        else if (abilityModify <= 0 && itemModify <= 0)
            m_clickAmount = ClickAmount;
    }

    public void OnClickButtonPressed()
    {
        //On Clicker first pressed
        m_lastClickedTime = DateTime.Now;
        IsClicked = true;
        OnClickedButton.Invoke(name);
    }

    void OnClickWithTimeRemaining(double secondsRemaining)
    {
        m_lastClickedTime = DateTime.Now - TimeSpan.FromSeconds(SecondsToCompleteClick - secondsRemaining);
        IsClicked = true;
        OnClickedButton.Invoke(name);
    }

    void CompletedClick()
    {
        //On Clicker timer complete
        m_sceneController.AddGold(ClickAmount);

        if(OnClickedFinished != null)
            OnClickedFinished.Invoke(name);

        m_sceneController.AddLastHit(1); //Add to global click count
        m_lastClickedTime = DateTime.MinValue;
    }

    void OnLoadedSaveFile(SaveFileDto saveFile)
    {
        
    }

    public void SetAbilityModifierAmount(double amount, int ability)
    {

    }

    public void RemoveAbilityModifierAmount(double amount)
    {
    }
}
