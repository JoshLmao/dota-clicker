﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections.Generic;

public class CrystalMaidenController : ClickerHeroBase
{
    protected override AudioClip[] AttackingResponses { get { return m_atkResponses; } }
    protected override Dictionary<int, AudioClip> AbilitySounds { get { return m_abilitySounds.ToDictionary(x => x.Key, x => x.Value); } }

    [SerializeField]
    private AudioClip[] m_atkResponses;

    [SerializeField]
    private IndexAbilityClipDict m_abilitySounds = null;

    private const string T_CRYSTAL_NOVA_ABILITY = "useCrystalNova";
    private const string T_FROSTBITE_ABILITY = "useFrostbite";
    private const string T_RARE_IDLE_ANIM = "doRareIdle";

    protected override void ApplyAbility(AbilityDto ability)
    {
        base.ApplyAbility(ability);

        if(ability.Index == 0)
        {
            //Crystal Nova
        }
        else if(ability.Index == 1)
        {
            //Frostbite
        }
    }
}
