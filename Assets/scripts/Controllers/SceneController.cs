﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SceneController : MonoBehaviour
{
    private static EventManager EventManager { get; set; }

    /// <summary>
    /// Total gold the player has
    /// </summary>
    public decimal TotalGold { get; private set; }
    /// <summary>
    /// Total last hits the player has
    /// </summary>
    public int LastHitAmount { get; private set; }

    public List<ClickerHeroBase> Heroes { get; set; }

    private PlayerItemsTray m_playerItems = null;

    private void Awake()
    {
        EventManager = this.gameObject.AddComponent<EventManager>();
        EventManager.AddListener<OnSellItem>(OnSellInventoryItem);

        // Initialize Static constructor
        var s = new GlobalSoundManager();

        // Set all canvases to interact with main camera
        Canvas[] canvases = FindObjectsOfType<Canvas>();
        foreach (Canvas c in canvases)
        {
            if (c.renderMode == RenderMode.WorldSpace)
                c.worldCamera = Camera.allCameras[0];
        }

        Heroes = FindObjectsOfType<ClickerHeroBase>().OrderBy(x => x.Hero).ToList();
        m_playerItems = FindObjectOfType<PlayerItemsTray>();
    }

    private void OnSellInventoryItem(OnSellItem e)
    {
        if(e.Hero == Hero.Unknown)
        {
            // Sold in player inventory
            m_playerItems.RemoveItem(e.SlotIndex);
        }
        else
        {
            ClickerHeroBase hero = Heroes.FirstOrDefault(x => x.Hero == e.Hero);
            hero.RemoveItem(e.SlotIndex);
        }

        // Refund half the cost
        AddGold(e.Item.Cost / 2);
    }

    private void Start ()
    {
        
    }

    public void AddLastHit(int addAmount)
    {
        LastHitAmount += addAmount;
    }

    public void AddGold(decimal amount)
    {
        if (amount <= 0)
            return;

        TotalGold += amount;
    }

    public void RemoveGold(decimal amount)
    {
        if (amount <= 0)
            return;

        decimal valueAfter = TotalGold - amount;
        if(valueAfter > 0)
            TotalGold -= amount;
    }

    public bool BuyHero(ClickerHeroBase heroClicker, Hero hero, int buyCost)
    {
        if (CanBuy(buyCost) && heroClicker.CanBuy())
        {
            RemoveGold(buyCost);

            heroClicker.BuyHero();
            EventManager.TriggerEvent(new OnHeroBought(heroClicker.Hero));

            return true;
        }
        Debug.Log($"Can't buy Hero '{heroClicker.Hero}'");
        return false;
    }

    public bool CanBuy(int cost)
    {
#if UNITY_EDITOR && DISABLE_GOLD_COST
        cost = 0;
#endif
        return TotalGold >= cost;
    }

    public void AddAbility(AbilityDto ability)
    {
        Heroes.FirstOrDefault(x => x.Hero == ability.Hero)?.AddAbility(ability);
    }

    public static EventManager GetEventManager()
    {
        return EventManager;
    }
}
