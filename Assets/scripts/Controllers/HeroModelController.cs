﻿using System;
using System.Collections;
using UnityEngine;

public class HeroModelController : MonoBehaviour
{
    public event Action AttackComplete;

    public ClickerHeroBase Hero { get; internal set; }

    private double AnimAttackTime = double.NaN;

    [SerializeField]
    private Collider m_heroCollider = null;

    private float m_attackCount = 0;
    private Animator m_animator = null;
    private bool m_isDoingOneAttack = false;
    private EventManager m_eventManager = null;

    private const string IS_ATTACKING = "isAttacking";
    private const string IS_BOUGHT = "isBought";

    private const float NORMAL_ANIMATOR_SPEED = 1f;

    private Coroutine m_idleAnimRoutine = null;

    #region MonoBehavious
    private void Awake()
    {
        m_animator = GetComponent<Animator>();
        m_heroCollider = GetComponentInChildren<Collider>();
        if (m_heroCollider == null)
            Debug.LogError($"Can't find collider for hero '{this.gameObject.name}'");
    }

    private void Start()
    {
        m_eventManager = SceneController.GetEventManager();

        ClickPropagator clickPropagator = m_heroCollider.gameObject.AddComponent<ClickPropagator>();
        clickPropagator.MouseDown += OnModelMouseDown;
        clickPropagator.MouseUp += OnModelMouseUp;
        clickPropagator.MouseEnter += OnModelMouseEnter;
        clickPropagator.MouseExit += OnModelMouseExit;

        AnimAttackTime = GetAttackAnimTime();

        if(Hero.IsBought)
            m_idleAnimRoutine = StartCoroutine(RareIdleAnimLoop(Constants.RareIdleAnimTime));
    }

    private void OnApplicationQuit()
    {
        ClickPropagator click = m_heroCollider.gameObject.GetComponent<ClickPropagator>();
        click.MouseDown -= OnModelMouseDown;
        click.MouseUp -= OnModelMouseUp;
        click.MouseEnter -= OnModelMouseEnter;
        click.MouseExit -= OnModelMouseExit;
    }
    #endregion

    public void OnCompletedAttack()
    {
        if (m_isDoingOneAttack)
        {
            m_isDoingOneAttack = false;
            m_animator.SetBool(IS_ATTACKING, false);
            m_animator.speed = NORMAL_ANIMATOR_SPEED;
        }

        m_attackCount = 0f;
        AttackComplete?.Invoke();
    }

    public void AutoAttack()
    {
        m_animator.speed = GetAnimatorSpeed();
        m_animator.SetBool(IS_ATTACKING, true);
    }

    public void StopAutoAttack()
    {
        m_animator.SetBool(IS_ATTACKING, false);
        m_animator.speed = 1f;
    }

    public void DoAttack()
    {
        if (m_isDoingOneAttack)
            return;

        m_isDoingOneAttack = true;

        m_animator.speed = GetAnimatorSpeed();
        m_animator.SetBool(IS_ATTACKING, true);
    }

    public void SetBought(bool isBought)
    {
        if (isBought)
        {
            m_animator.SetTrigger(IS_BOUGHT);
        }

        // Set all albedo colors to dark when inactive
        SkinnedMeshRenderer[] meshes = GetComponentsInChildren<SkinnedMeshRenderer>();
        foreach (SkinnedMeshRenderer mesh in meshes)
            foreach (Material mat in mesh.materials)
                mat.color = isBought ? Color.white : new Color(0.2f, 0.2f, 0.2f);

        // Start idle anim routine
        if (m_idleAnimRoutine == null)
            m_idleAnimRoutine = StartCoroutine(RareIdleAnimLoop(Constants.RareIdleAnimTime));
    }

    private void OnModelMouseEnter(GameObject gameObj)
    {
        m_eventManager.TriggerEvent(new HeroMouseEnter(Hero.Hero));
    }

    private void OnModelMouseExit(GameObject gameObj)
    {
        m_eventManager.TriggerEvent(new HeroMouseExit(Hero.Hero)); 
    }

    private void OnModelMouseUp(GameObject gameObj)
    {
        m_eventManager.TriggerEvent(new HeroMouseUp(Hero.Hero));
    }

    private void OnModelMouseDown(GameObject gameObj)
    {
        m_eventManager.TriggerEvent(new HeroMouseDown(Hero.Hero));
    }

    private float GetAnimatorSpeed()
    {
        double atkTime = Hero.GetAttackTime();
        return (float)(AnimAttackTime / atkTime);
    }

    private double GetAttackAnimTime()
    {
        var ac = m_animator.runtimeAnimatorController as UnityEditor.Animations.AnimatorController;
        var sm = ac.layers[0].stateMachine;

        double animAtkTime = double.NaN;
        for (int i = 0; i < sm.states.Length; i++)
        {
            var state = sm.states[i];
            if (state.state.name.ToLower().Contains("attack"))
            {
                AnimationClip clip = state.state.motion as AnimationClip;
                if (clip != null)
                {
                    animAtkTime = clip.length;
                    break;
                }
            }
        }
        if (double.IsNaN(animAtkTime))
            Debug.LogError($"Unable to find attack animation time for hero '{Hero.Hero}'");

        return animAtkTime;
    }

    private IEnumerator RareIdleAnimLoop(float time)
    {
        yield return new WaitForSeconds(time);
        if (!Hero.IsAutoAttacking)
        {
            m_animator.SetTrigger("doRareIdle");

            m_idleAnimRoutine = StartCoroutine(RareIdleAnimLoop(Constants.RareIdleAnimTime));
        }
    }
}
