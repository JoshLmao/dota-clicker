﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(RoshanEventManager))]
public class RoshanEventManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        RoshanEventManager roshanManager = (RoshanEventManager)target;
        if (GUILayout.Button("Start Roshan Event"))
        {
            roshanManager.StartEvent();
        }
    }
}
